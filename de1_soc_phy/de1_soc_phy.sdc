
create_clock -period 20 -name sys_clk [get_ports clk50]
create_clock -period 20 -name rmii_clk [get_ports eth_clk]
derive_pll_clocks

set_clock_groups -exclusive \
		-group {sys_clk} \
		-group {rmii_clk} \
		-group {altera_reserved_tck} \
		-group {inst_pll|pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk}

set_output_delay  -max -4  -clock inst_pll|pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk [get_ports {eth_tx_en eth_tx_d[*]}]
set_output_delay  -min -2 -clock inst_pll|pll_inst|altera_pll_i|general[0].gpll~PLL_OUTPUT_COUNTER|divclk [get_ports {eth_tx_en eth_tx_d[*]}] -add_delay

set_input_delay  -max 14  -clock rmii_clk [get_ports {eth_rx_crs eth_rx_d[*]}] -add_delay 
set_input_delay  -min 12 -clock rmii_clk [get_ports {eth_rx_crs eth_rx_d[*]}] -add_delay 

set_false_path -from [get_ports eth_mdio] -to *
set_false_path -from [get_ports altera_reserved_tdi] -to *
set_false_path -from [get_ports altera_reserved_tms] -to *
set_false_path -from * -to [get_ports altera_reserved_tdo]