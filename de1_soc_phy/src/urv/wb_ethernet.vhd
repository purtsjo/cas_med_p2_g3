-------------------------------------------------------------------------------
-- Title      : Wishbone Ethernet Interface
-- Project    : General Cores Collection (gencores) library
-------------------------------------------------------------------------------
-- File       : wb_ethernet.vhd
-- Author     : 
-- Company    : 
-- Created    :
-- Last update:
-- Platform   : FPGA-generics
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c)
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author          Description
-- 2016-01-18  1.0      bucher
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;

--use work.genram_pkg.all;
use work.wishbone_pkg.all;

entity wb_ethernet is
  generic(
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD
    );
  port (

    clk_sys_i       : in std_logic;
    rst_n_i         : in std_logic;
  
    wb_adr_i        : in  std_logic_vector(4 downto 0);
    wb_dat_i        : in  std_logic_vector(31 downto 0);
    wb_dat_o        : out std_logic_vector(31 downto 0);
    wb_cyc_i        : in  std_logic;
    wb_sel_i        : in  std_logic_vector(3 downto 0);
    wb_stb_i        : in  std_logic;
    wb_we_i         : in  std_logic;
    wb_ack_o        : out std_logic;
    wb_stall_o      : out std_logic;
  
    eth_control_i   : in  std_logic_vector(31 downto 0);
    eth_counter_i   : in  std_logic_vector(31 downto 0);
    eth_error_i     : in  std_logic_vector(31 downto 0);
    eth_buffer_o    : out std_logic_vector(31 downto 0);
    eth_data_rdy_o  : out std_logic
    );
end wb_ethernet;

architecture syn of wb_ethernet is
  
  constant c_ETH_REG_CTL  : std_logic_vector(2 downto 0) := "000";  -- control register
  constant c_ETH_REG_CNT  : std_logic_vector(2 downto 0) := "001";  -- packet counter
  constant c_ETH_REG_ERR  : std_logic_vector(2 downto 0) := "010";  -- error counter
  constant c_ETH_REG_OUT  : std_logic_vector(2 downto 0) := "011";  -- output buffer
 
  signal control, control_meta    : std_logic_vector(31 downto 0);
  signal counter, counter_meta    : std_logic_vector(31 downto 0);
  signal error, error_meta        : std_logic_vector(31 downto 0);
  signal buffer_out               : std_logic_vector(31 downto 0);
  signal data_ready               : std_logic_vector( 3 downto 0);

  signal resized_addr : std_logic_vector(c_wishbone_address_width-1 downto 0);

  signal wb_in  : t_wishbone_slave_in;
  signal wb_out : t_wishbone_slave_out;
  signal sel : std_logic;
  signal ack_int : std_logic;
  
begin  -- syn

  resized_addr(4 downto 0)                          <= wb_adr_i;
  resized_addr(c_wishbone_address_width-1 downto 5) <= (others => '0');

  U_Adapter : wb_slave_adapter
    generic map (
      g_master_use_struct  => true,
      g_master_mode        => CLASSIC,
      g_master_granularity => WORD,
      g_slave_use_struct   => false,
      g_slave_mode         => g_interface_mode,
      g_slave_granularity  => g_address_granularity)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_i,
      master_i   => wb_out,
      master_o   => wb_in,
      sl_adr_i   => resized_addr,
      sl_dat_i   => wb_dat_i,
      sl_sel_i   => wb_sel_i,
      sl_cyc_i   => wb_cyc_i,
      sl_stb_i   => wb_stb_i,
      sl_we_i    => wb_we_i,
      sl_dat_o   => wb_dat_o,
      sl_ack_o   => wb_ack_o,
      sl_stall_o => wb_stall_o);
      
  sel <= '1' when (unsigned(not wb_in.sel) = 0) else '0';
  
  p_wb_write: process (clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        control 		<= (others => '0'); 	control_meta <= (others => '0');
        counter 		<= (others => '0'); 	counter_meta <= (others => '0');
        error   		<= (others => '0'); 	error_meta   <= (others => '0');
        buffer_out   <= (others => '0');
        data_ready   <= (others => '0');
      else
        -- default assignments
        data_ready <= data_ready(2 downto 0) & '0';
      
        if(wb_in.we = '1' and wb_in.cyc = '1' and wb_in.stb = '1' and sel = '1') then
          if (wb_in.adr(2 downto 0) = c_ETH_REG_OUT) then
            buffer_out <= wb_in.dat;
            data_ready <= (others => '1');
          end if;
        end if;
        
        -- clock domain crossings
        control <= control_meta;  control_meta <= eth_control_i;
        counter <= counter_meta;  counter_meta <= eth_counter_i;
        error   <= error_meta;    error_meta   <= eth_error_i;
      end if;
    end if;
  end process;

  p_wb_reads: process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        wb_out.dat <= (others => '0');
      else
        wb_out.dat <= (others => 'X');
        case wb_in.adr(2 downto 0) is
          when c_ETH_REG_CTL =>   wb_out.dat <= control;
          when c_ETH_REG_CNT =>   wb_out.dat <= counter;  
          when c_ETH_REG_ERR =>   wb_out.dat <= error;
          when c_ETH_REG_OUT =>   wb_out.dat <= buffer_out; 
          when others => null;
        end case;
      end if;
    end if;
  end process;
  
  p_gen_ack : process (clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if rst_n_i = '0' then
        ack_int <= '0';
      else
        if(ack_int = '1') then
          ack_int <= '0';
        elsif(wb_in.cyc = '1') and (wb_in.stb = '1') then
          ack_int <= '1';
        end if;
      end if;
    end if;
  end process;    
  
  -- port mappings
  eth_data_rdy_o <= data_ready(3);
  eth_buffer_o   <= buffer_out;
   
  wb_out.ack   <= ack_int;
  wb_out.stall <= '0';
  wb_out.err   <= '0';
  wb_out.int   <= '0';
  wb_out.rty   <= '0';

end syn;
