library ieee;
use ieee.std_logic_1164.all;

use work.wishbone_pkg.all;

entity xwb_ethernet is
  generic(
    g_interface_mode         : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity    : t_wishbone_address_granularity := WORD  
  );
  port(
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    -- Wishbone
    slave_i   : in  t_wishbone_slave_in;
    slave_o   : out t_wishbone_slave_out;
    desc_o    : out t_wishbone_device_descriptor;

    eth_control_i   : in  std_logic_vector(31 downto 0);
    eth_counter_i   : in  std_logic_vector(31 downto 0);
    eth_error_i     : in  std_logic_vector(31 downto 0);
    eth_buffer_o    : out std_logic_vector(31 downto 0);
    eth_data_rdy_o  : out std_logic
  );
end xwb_ethernet;

architecture rtl of xwb_ethernet is

  component wb_ethernet is
  generic(
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD
    );
  port (

    clk_sys_i       : in std_logic;
    rst_n_i         : in std_logic;
  
    wb_adr_i        : in  std_logic_vector(4 downto 0);
    wb_dat_i        : in  std_logic_vector(31 downto 0);
    wb_dat_o        : out std_logic_vector(31 downto 0);
    wb_cyc_i        : in  std_logic;
    wb_sel_i        : in  std_logic_vector(3 downto 0);
    wb_stb_i        : in  std_logic;
    wb_we_i         : in  std_logic;
    wb_ack_o        : out std_logic;
    wb_stall_o      : out std_logic;
  
    eth_control_i   : in  std_logic_vector(31 downto 0);
    eth_counter_i   : in  std_logic_vector(31 downto 0);
    eth_error_i     : in  std_logic_vector(31 downto 0);
    eth_buffer_o    : out std_logic_vector(31 downto 0);
    eth_data_rdy_o  : out std_logic
    );
  end component wb_ethernet;
  
begin  -- rtl
  

  Wrapped_ETH : wb_ethernet
    generic map (
      g_interface_mode         => g_interface_mode,
      g_address_granularity    => g_address_granularity)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_i,
      wb_sel_i   => slave_i.sel,
      wb_cyc_i   => slave_i.cyc,
      wb_stb_i   => slave_i.stb,
      wb_we_i    => slave_i.we,
      wb_adr_i   => slave_i.adr(4 downto 0),
      wb_dat_i   => slave_i.dat(31 downto 0),
      wb_dat_o   => slave_o.dat(31 downto 0),
      wb_ack_o   => slave_o.ack,
      wb_stall_o => slave_o.stall,

      eth_control_i   => eth_control_i,
      eth_counter_i   => eth_counter_i,
      eth_error_i     => eth_error_i,
      eth_buffer_o    => eth_buffer_o,
      eth_data_rdy_o  => eth_data_rdy_o);

  slave_o.err   <= '0';
  slave_o.int   <= '0';
  slave_o.rty   <= '0';
  
end rtl;
