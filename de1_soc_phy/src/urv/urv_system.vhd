-- 
-- uRV - a tiny and dumb RISC-V core
-- Copyright (c) 2015 CERN
-- Author: Tomasz Włostowski <tomasz.wlostowski@cern.ch>

-- This library is free software; you can redistribute it and/or
-- modify it under the terms of the GNU Lesser General Public
-- License as published by the Free Software Foundation; either
-- version 3 of the License, or (at your option) any later version.
--
-- This library is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this library; if not, write to the Free Software
-- Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
-- 

--
-- rev1_top.vhd - top level for rev 1.1. PCB FPGA
--
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;


entity urv_system is
  generic (
    g_riscv_firmware : string  := "hello.mif";
    g_riscv_mem_size : integer := 65536;
    g_simulation : boolean := false
    );
  port (
    rst_sys_i       : in  std_logic;
    rst_cpu_i       : in  std_logic;
    clk_sys_i       : in  std_logic;  -- 100 MHz
    uart_txd_o      : out std_logic;
    uart_rxd_i      : in  std_logic;
	  gpio_out_o      : out std_logic_vector(31 downto 0);
    spi_cs_o        : out std_logic_vector(7 downto 0);
    spi_sclk_o      : out std_logic;
    spi_mosi_o      : out std_logic;
    spi_miso_i      : in  std_logic;
    dm_data_eth_i   : in  std_logic_vector(31 downto 0);
    dm_data_eth_o   : out std_logic_vector(31 downto 0);
    dm_addr_eth_i   : in  std_logic_vector(31 downto 0);
    dm_we_eth_i     : in  std_logic;
    eth_control_i   : in  std_logic_vector(31 downto 0);
    eth_counter_i   : in  std_logic_vector(31 downto 0);
    eth_error_i     : in  std_logic_vector(31 downto 0);
    eth_buffer_o    : out std_logic_vector(31 downto 0);
    eth_data_rdy_o  : out std_logic
  );
end urv_system;

architecture rtl of urv_system is

  component xurv_core is
    generic (
      g_internal_ram_size      : integer;
      g_internal_ram_init_file : string;
      g_simulation : boolean;
      g_address_bits           : integer);
    port (
      clk_sys_i     : in  std_logic;
      rst_n_i       : in  std_logic;
      cpu_rst_i     : in  std_logic                    := '0';
      irq_i         : in  std_logic_vector(7 downto 0) := x"00";
      dwb_o         : out t_wishbone_master_out;
      dwb_i         : in  t_wishbone_master_in;
      host_slave_i  : in  t_wishbone_slave_in          := cc_dummy_slave_in;
      host_slave_o  : out t_wishbone_slave_out;
      dm_data_eth_i : in  std_logic_vector(31 downto 0);
      dm_data_eth_o : out std_logic_vector(31 downto 0);
      dm_addr_eth_i : in  std_logic_vector(31 downto 0);
      dm_we_eth_i   : in  std_logic);
  end component xurv_core;
  
  component xwb_ethernet
  generic(
    g_interface_mode         : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity    : t_wishbone_address_granularity := WORD  
  );
  port(
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;
    -- Wishbone
    slave_i   : in  t_wishbone_slave_in;
    slave_o   : out t_wishbone_slave_out;
    desc_o    : out t_wishbone_device_descriptor;
    eth_control_i   : in  std_logic_vector(31 downto 0);
    eth_counter_i   : in  std_logic_vector(31 downto 0);
    eth_error_i     : in  std_logic_vector(31 downto 0);
    eth_buffer_o    : out std_logic_vector(31 downto 0);
    eth_data_rdy_o  : out std_logic
  );
  end component xwb_ethernet;

  constant c_cnx_slave_ports  : integer := 1;
  constant c_cnx_master_ports : integer := 4;

  constant c_master_cpu       : integer := 0;

  constant c_slave_gpio       : integer := 0;
  constant c_slave_uart       : integer := 1;
  constant c_slave_spi        : integer := 2;
  constant c_slave_eth        : integer := 3;  

  signal cnx_slave_in  : t_wishbone_slave_in_array(c_cnx_slave_ports-1 downto 0);
  signal cnx_slave_out : t_wishbone_slave_out_array(c_cnx_slave_ports-1 downto 0);

  signal cnx_master_in  : t_wishbone_master_in_array(c_cnx_master_ports-1 downto 0);
  signal cnx_master_out : t_wishbone_master_out_array(c_cnx_master_ports-1 downto 0);

  constant c_cfg_base_addr : t_wishbone_address_array(c_cnx_master_ports-1 downto 0) :=
    (c_slave_eth  => x"80003000",                  -- Ethernet
     c_slave_spi  => x"80002000",                  -- SPI
     c_slave_gpio => x"80001000",                  -- GPIO
     c_slave_uart => x"80000000");                 -- UART

  constant c_cfg_base_mask : t_wishbone_address_array(c_cnx_master_ports-1 downto 0) :=
    (c_slave_eth  => x"8000f000",
     c_slave_spi  => x"8000f000",
     c_slave_gpio => x"8000f000",
     c_slave_uart => x"8000f000" );


--  signal pllout_clk_fb_pllref, pllout_clk_sys, clk_sys, sys_locked, sys_locked_n : std_logic;
  signal  rst_n_sys : std_logic;

  signal dummy, gpio_in, gpio_oen : std_logic_vector(31 downto 0);

begin  -- rtl
 
  rst_n_sys <= not rst_sys_i;


  U_CPU: xurv_core
    generic map (
      g_internal_ram_size      => g_riscv_mem_size,
      g_internal_ram_init_file => g_riscv_firmware,
      g_simulation => g_simulation,
      g_address_bits           => 32)
    port map (
      clk_sys_i     => clk_sys_i,
      rst_n_i       => rst_n_sys,
      cpu_rst_i     => rst_cpu_i,
      dwb_o         => cnx_slave_in(0),
      dwb_i         => cnx_slave_out(0),     
      dm_data_eth_i => dm_data_eth_i,
      dm_data_eth_o => dm_data_eth_o,
      dm_addr_eth_i => dm_addr_eth_i,
      dm_we_eth_i   => dm_we_eth_i);
 
  U_Intercon : xwb_crossbar
    generic map (
      g_num_masters => c_cnx_slave_ports,
      g_num_slaves  => c_cnx_master_ports,
      g_registered  => true,
      g_address     => c_cfg_base_addr,
      g_mask        => c_cfg_base_mask)
    port map (
      clk_sys_i => clk_sys_i,
      rst_n_i   => rst_n_sys,
      slave_i   => cnx_slave_in,
      slave_o   => cnx_slave_out,
      master_i  => cnx_master_in,
      master_o  => cnx_master_out);
  
  U_ETH: xwb_ethernet
  generic map (
      g_interface_mode      => PIPELINED,
      g_address_granularity => BYTE
  )
  port map (
    clk_sys_i  => clk_sys_i,
    rst_n_i    => rst_n_sys,
    slave_i    => cnx_master_out(c_slave_eth),
    slave_o    => cnx_master_in(c_slave_eth),

    eth_control_i  => eth_control_i, 
    eth_counter_i  => eth_counter_i,
    eth_error_i    => eth_error_i,   
    eth_buffer_o   => eth_buffer_o,  
    eth_data_rdy_o => eth_data_rdy_o
  );
  
  U_UART : xwb_simple_uart
    generic map (
      g_interface_mode      => PIPELINED,
      g_address_granularity => BYTE)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_sys,
      slave_i    => cnx_master_out(c_slave_uart),
      slave_o    => cnx_master_in(c_slave_uart),
      uart_rxd_i => uart_rxd_i,
      uart_txd_o => uart_txd_o);
 
 U_GPIO : xwb_gpio_port
    generic map (
      g_interface_mode         => PIPELINED,
      g_address_granularity    => BYTE,
      g_num_pins               => 32,
      -- we don't want a 3-state output
      g_with_builtin_tristates => false)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_sys,
      slave_i    => cnx_master_out(c_slave_gpio),
      slave_o    => cnx_master_in(c_slave_gpio),
      gpio_b     => dummy,
      gpio_out_o => gpio_out_o,
      gpio_in_i  => gpio_in,
      gpio_oen_o => gpio_oen);
      
  U_Wrapped_SPI: wb_spi
    generic map (
      g_interface_mode      => PIPELINED,
      g_address_granularity => BYTE)
    port map (
      clk_sys_i  => clk_sys_i,
      rst_n_i    => rst_n_sys,
      wb_adr_i   => cnx_master_out(c_slave_spi).adr(4 downto 0),
      wb_dat_i   => cnx_master_out(c_slave_spi).dat,
      wb_dat_o   => cnx_master_in(c_slave_spi).dat,
      wb_sel_i   => cnx_master_out(c_slave_spi).sel,
      wb_stb_i   => cnx_master_out(c_slave_spi).stb,
      wb_cyc_i   => cnx_master_out(c_slave_spi).cyc,
      wb_we_i    => cnx_master_out(c_slave_spi).we,
      wb_ack_o   => cnx_master_in(c_slave_spi).ack,
      wb_err_o   => cnx_master_in(c_slave_spi).err,
      wb_int_o   => cnx_master_in(c_slave_spi).int,
      wb_stall_o => cnx_master_in(c_slave_spi).stall,
      pad_cs_o   => spi_cs_o,
      pad_sclk_o => spi_sclk_o,
      pad_mosi_o => spi_mosi_o,
      pad_miso_i => spi_miso_i);
 
end rtl;

