
----------------------------------------------------------------------
--
--
--
----------------------------------------------------------------------
--

library ieee;
use ieee.std_logic_1164.all;    
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.PCK_CRC32_D8.all;
--USE ieee.numeric_std_unsigned.all;

entity eth_mac is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- RMII interface
        rmii_tx_clk_i    : in std_logic;
        rmii_rx_clk_i    : in std_logic;
        rmii_tx_en_o  : out std_logic;
        rmii_tx_d_o   : out std_logic_vector(1 downto 0);
        rmii_rx_d_i   : in std_logic_vector(1 downto 0);
        rmii_rx_crs_i : in std_logic;
        -- system interface to framebuffer:
        fb_rx_addr_i  : in std_logic_vector(9 downto 0);
        fb_rx_data_o  : out std_logic_vector(31 downto 0);
        fb_rx_done_i  : in std_logic;
        fb_rx_ready_o : out std_logic;

        fb_tx_addr_i  : in std_logic_vector(9 downto 0);
        fb_tx_data_i  : in std_logic_vector(31 downto 0);
        fb_tx_write_i : in std_logic;
        fb_tx_done_i  : in std_logic;
        fb_tx_ready_o : out std_logic
        
    );
end eth_mac;

architecture rtl of eth_mac is

component framebuffer_rx is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- MAC interface:
       rmii_clk_i  : in std_logic;
        sof_i   : in std_logic;
        eof_i   : in std_logic;
        octet_i : in std_logic_vector(7 downto 0);
        octet_en_i : in std_logic;
        length_i : in std_logic_vector(10 downto 0);
        -- System interface:
        fb_addr_i : in std_logic_vector(9 downto 0);
        fb_data_o : out std_logic_vector(31 downto 0);
        fb_done_i : in std_logic;
        fb_ready_o : out std_logic
    );
end component;

component framebuffer_tx is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- MAC interface:
        rmii_clk_i : in std_logic;
        ready_o    : out std_logic;
        valid_o    : out std_logic;
        start_i    : in std_logic;
        eof_o      : out std_logic;
        octet_ack_i : in std_logic;
        octet_o    : out std_logic_vector(7 downto 0);
        -- System interface:
        fb_addr_i : in std_logic_vector(9 downto 0);
        fb_data_i : in std_logic_vector(31 downto 0);
        fb_write_i : in std_logic;
        fb_done_i : in std_logic;
        fb_ready_o : out std_logic
    );
end component;
    
    type state_type is (idle, waitPreamble, waitSFD, payload);
    type state_tx_type is (wait_ready, send_zero, send_preamble, send_payload, send_padding, send_FCS, send_SFD, IPG, done);
                      
    signal fsm_state : state_type;
    signal fsm_tx_state : state_tx_type;
    signal octet_shift : std_logic_vector(7 downto 0);
    signal octet_en : std_logic;
    signal octet_cnt : unsigned(3 downto 0);
    signal tx_cnt : unsigned(5 downto 0);
    signal rmii_rx_crs : std_logic;
    signal sof, eof : std_logic;
    signal length_cnt : unsigned(10 downto 0);
    signal tx_length_cnt : unsigned(10 downto 0);
    signal octet_ack : std_logic;
    signal crc_reg, crc_inv : std_logic_vector(31 downto 0);
    
    -- framebuffer tx signals:
    signal tx_ready, tx_eof, tx_start, tx_valid : std_logic;
    signal tx_octet : std_logic_vector(7 downto 0);
begin

fb_rx_inst : framebuffer_rx
    port map (
        sys_clk_i     => sys_clk_i,
        sys_rst       => sys_rst,
        -- MAC interface:
        rmii_clk_i  => rmii_rx_clk_i,
        sof_i       => sof,
        eof_i       => eof,
        octet_i     => octet_shift,
        octet_en_i  => octet_en,
        length_i    => std_logic_vector(length_cnt),

        -- System interface:
        fb_addr_i   => fb_rx_addr_i,
        fb_data_o   => fb_rx_data_o,
        fb_done_i   => fb_rx_done_i,
        fb_ready_o  => fb_rx_ready_o
    );
    
fb_tx_inst : framebuffer_tx
    port map (
        sys_clk_i   => sys_clk_i,
        sys_rst     => sys_rst,
        -- MAC interface:
        rmii_clk_i  => rmii_tx_clk_i,
        ready_o    => tx_ready,
        valid_o    => tx_valid,
        eof_o      => tx_eof,
        start_i    => tx_start,
        octet_ack_i => octet_ack,
        octet_o    => tx_octet,
        -- System interface:
        fb_addr_i   => fb_tx_addr_i,
        fb_data_i   => fb_tx_data_i,
        fb_write_i  => fb_tx_write_i,
        fb_done_i   => fb_tx_done_i,
        fb_ready_o  => fb_tx_ready_o
    );
    
   
----------------------------------------------------------
-- process: RX MAC
----------------------------------------------------------
fsm_rx: process(all)
begin
    if (sys_rst = '1') then
        fsm_state <= idle;
        octet_en <= '0';
        octet_cnt <= (others=>'0');
        octet_shift <= (others=>'0');
        length_cnt <= (others=>'0');
        rmii_rx_crs <= '0';
        eof <= '0';
        sof <= '0';
    elsif(rising_edge(rmii_rx_clk_i)) then
        octet_shift <=  rmii_rx_d_i & octet_shift (7 downto 2);
        octet_en <= '0';
        rmii_rx_crs <= rmii_rx_crs_i;
        
        case fsm_state is
            when idle =>
                sof <= '0';
                eof <= '0';
                octet_cnt  <= (others=>'0');
                length_cnt <= (others=>'0');

                if (rmii_rx_crs = '1') then
                    fsm_state <= waitPreamble;
                end if;
            
            when waitPreamble =>
                if (octet_shift = x"55") then
                    if (octet_cnt < 7) then
                        octet_cnt <= octet_cnt + 1;
                    else
                        fsm_state <= waitSFD;
                    end if;
                else
                    octet_cnt <= (others=>'0');
                end if;
                if (rmii_rx_crs = '0') then
                   fsm_state <= idle; 
                end if;
                
            when waitSFD =>
                sof <= '0';
                octet_cnt <= (others=>'0');
                if (octet_shift = x"55") then
                    fsm_state <= fsm_state;
                elsif (octet_shift = x"D5") then
                    octet_cnt <= "0001";
                    fsm_state <= payload;
                    sof <= '1';
                else
                    fsm_state <= waitPreamble;
                end if;
                if (rmii_rx_crs = '0') then
                   fsm_state <= idle; 
                end if;

            when payload =>
                sof <= '0';
                octet_en <= '0';
                if (octet_cnt = 3) then
                    length_cnt <= length_cnt + 1;
                    octet_en <= '1';
                    octet_cnt <= (others=>'0');
                else
                    octet_cnt <= octet_cnt + 1;
                end if;
                if (rmii_rx_crs = '0') then
                   fsm_state <= idle; 
                   eof <= '1';
                end if;
                
        end case;
    end if;
end process;

----------------------------------------------------------
-- process: TX MAC
----------------------------------------------------------
fsm_tx: process(all)
    variable data_out : std_logic_vector(7 downto 0);
begin
    if (sys_rst = '1') then
        tx_start <= '0';
        fsm_tx_state <= wait_ready;
        rmii_tx_en_o <= '0';
        rmii_tx_d_o <= "00";
        tx_cnt <= (others=>'0');
        octet_ack <= '0';
        tx_length_cnt <= (others=>'0');
        crc_reg <= (others=>'1');
        crc_inv <= (others=>'0');
    elsif(rising_edge(rmii_tx_clk_i)) then
        tx_start <= '0';
        data_out := x"00";

        case fsm_tx_state is
            -------------------------------------------------
            when wait_ready =>
                if (tx_ready = '1') then
                    fsm_tx_state <= send_preamble;
                end if;    
                tx_cnt <= (others=>'0');
            
            when send_zero =>
                if (tx_cnt < 6-1) then
                    tx_cnt <= tx_cnt + 1;
                else
                    tx_cnt <= (others=>'0');
                    fsm_tx_state <= send_preamble;
                end if;
                rmii_tx_en_o <= '1';
                data_out := x"00";
            
            -------------------------------------------------
            when send_preamble =>
                if (tx_cnt < 28-1) then
                --if (tx_cnt < 34-1) then
                    tx_cnt <= tx_cnt + 1;
                else
                    tx_cnt <= (others=>'0');
                    fsm_tx_state <= send_SFD;
                end if;
                rmii_tx_en_o <= '1';
                data_out := x"55";
                
            -------------------------------------------------
            when send_SFD =>
                if (tx_cnt < 3) then
                    tx_cnt <= tx_cnt + 1;
                else
                    tx_cnt <= (others=>'0');
                    fsm_tx_state <= send_payload;
                end if;
                rmii_tx_en_o <= '1';
                data_out := x"D5";
                tx_length_cnt <= (others=>'0');
                
            -------------------------------------------------
            when send_payload =>
                octet_ack <= '0';
                data_out := tx_octet;
                if (tx_cnt = 3) then
                    tx_cnt <= (others=>'0');
                    tx_length_cnt <= tx_length_cnt + 1;
                else
                    tx_cnt <= tx_cnt + 1;
                end if;
                if (tx_cnt = 2) then
                    octet_ack <= '1';
                end if;
                rmii_tx_en_o <= '1';
                
                if (tx_eof = '1') then
                    if (tx_length_cnt < 60) then
                            data_out := x"00"; -- first padding byte
                            fsm_tx_state <= send_padding;
                    else
                            tx_length_cnt <= (others=>'0');
                            data_out := crc_reg(31 downto 24); -- first padding byte
                            fsm_tx_state <= send_FCS;
                    end if;
                end if;
                
            -------------------------------------------------
            when send_padding =>
                if (tx_cnt = 3) then
                    tx_cnt <= (others=>'0');
                    tx_length_cnt <= tx_length_cnt + 1;
                else
                    tx_cnt <= tx_cnt + 1;
                end if;
            
                if (tx_length_cnt < 60) then
                    rmii_tx_en_o <= '1';
                    data_out := x"00";
                else
                    tx_length_cnt <= (others=>'0');
                    data_out := crc_inv(31 downto 24);
                    fsm_tx_state <= send_FCS;
                end if;
                
            -------------------------------------------------
            when send_FCS =>
                rmii_tx_en_o <= '1';
                data_out := crc_inv(31 downto 24);
                --data_out := x"FF";
                if (tx_cnt = 3) then
                    crc_inv <= crc_inv(23 downto 0) & x"00";
                    tx_length_cnt <= tx_length_cnt + 1;
                    tx_cnt <= (others=>'0');
                else
                    tx_cnt <= tx_cnt + 1;
                end if;
                --if (tx_length_cnt = 3 and tx_cnt = 3) then
                --    rmii_tx_en_o <= '0';
                --end if;
                if (tx_length_cnt = 4) then
                    rmii_tx_en_o <= '0';
                    data_out := x"00";
                    fsm_tx_state <= IPG;
                end if;
                
            -------------------------------------------------
            when IPG =>
                rmii_tx_en_o <= '0';
                if (tx_length_cnt = 4*12) then
                    tx_length_cnt <= (others=>'0');
                    fsm_tx_state <= done;
                else
                    tx_length_cnt <= tx_length_cnt + 1;
                end if;
                
            -------------------------------------------------
            when done =>
                rmii_tx_en_o <= '0';
                fsm_tx_state <= wait_ready;
                
        end case;
        
        -----------------------------------------------
        if ((fsm_tx_state = send_payload or fsm_tx_state = send_padding)) then
            if (tx_cnt(1 downto 0) = "10") then
                crc_reg <= nextCRC32_D8(data_out(0) &
                                        data_out(1) &
                                        data_out(2) &
                                        data_out(3) &
                                        data_out(4) &
                                        data_out(5) &
                                        data_out(6) &
                                        data_out(7) , crc_reg);
            else
                crc_reg <= crc_reg;
            end if;
                    crc_inv(0) <= not crc_reg(7);
                    crc_inv(1) <= not crc_reg(6);
                    crc_inv(2) <= not crc_reg(5);
                    crc_inv(3) <= not crc_reg(4);
                    crc_inv(4) <= not crc_reg(3);
                    crc_inv(5) <= not crc_reg(2);
                    crc_inv(6) <= not crc_reg(1);
                    crc_inv(7) <= not crc_reg(0);
                    crc_inv(8) <= not crc_reg(15);
                    crc_inv(9) <= not crc_reg(14);
                    crc_inv(10) <= not crc_reg(13);
                    crc_inv(11) <= not crc_reg(12);
                    crc_inv(12) <= not crc_reg(11);
                    crc_inv(13) <= not crc_reg(10);
                    crc_inv(14) <= not crc_reg(9);
                    crc_inv(15) <= not crc_reg(8);
                    crc_inv(16) <= not crc_reg(23);
                    crc_inv(17) <= not crc_reg(22);
                    crc_inv(18) <= not crc_reg(21);
                    crc_inv(19) <= not crc_reg(20);
                    crc_inv(20) <= not crc_reg(19);
                    crc_inv(21) <= not crc_reg(18);
                    crc_inv(22) <= not crc_reg(17);
                    crc_inv(23) <= not crc_reg(16);
                    crc_inv(24) <= not crc_reg(31);
                    crc_inv(25) <= not crc_reg(30);
                    crc_inv(26) <= not crc_reg(29);
                    crc_inv(27) <= not crc_reg(28);
                    crc_inv(28) <= not crc_reg(27);
                    crc_inv(29) <= not crc_reg(26);
                    crc_inv(30) <= not crc_reg(25);
                    crc_inv(31) <= not crc_reg(24);
        elsif (fsm_tx_state = wait_ready) then
            crc_reg <= (others=>'1');
        else
            crc_reg <= crc_reg;
        end if;
        
        
        -----------------------------------------------
        case tx_cnt(1 downto 0) is
            when "00" =>
                rmii_tx_d_o <= data_out(1 downto 0);
            when "01" =>
                rmii_tx_d_o <= data_out(3 downto 2);
            when "10" =>
                rmii_tx_d_o <= data_out(5 downto 4);
            when "11" =>
                rmii_tx_d_o <= data_out(7 downto 6);
            when others=>
                rmii_tx_d_o <= "00";
        end case;
        
    end if;
end process;

end rtl;


