
----------------------------------------------------------------------
--
--
--
----------------------------------------------------------------------
--

library ieee;
use ieee.std_logic_1164.all;    
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
--USE ieee.numeric_std_unsigned.all;

entity framebuffer_tx is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- MAC interface:
        rmii_clk_i : in std_logic;
        ready_o    : out std_logic;
        valid_o    : out std_logic;
        start_i    : in std_logic;
        eof_o      : out std_logic;
        octet_ack_i : in std_logic;
        octet_o    : out std_logic_vector(7 downto 0);
        -- System interface:
        fb_addr_i : in std_logic_vector(9 downto 0);
        fb_data_i : in std_logic_vector(31 downto 0);
        fb_write_i : in std_logic;
        fb_done_i  : in std_logic;
        fb_ready_o : out std_logic
    );
end framebuffer_tx;


architecture rtl of framebuffer_tx is

component dp_ram
	PORT
	(
		address_a   : IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		address_b	: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		clock_a		: IN STD_LOGIC  := '1';
		clock_b		: IN STD_LOGIC ;
		data_a		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data_b		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		wren_a		: IN STD_LOGIC  := '0';
		wren_b		: IN STD_LOGIC  := '0';
		q_a		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		q_b		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;

type fsm_wr_type is (init, check_status, check_status_wait, wait_done, done);
type fsm_rd_type is (init, poll_ready, octet_transfer, free_buffer, waitstate, done, octet_1, octet_2, octet_3, octet_4);

signal fsm_wr_state : fsm_wr_type;
signal fsm_rd_state : fsm_rd_type;

signal fb_tx_wr_addr, fb_tx_rd_addr : std_logic_vector(9 downto 0);
signal fb_tx_wr_data_in, fb_tx_rd_data_in, fb_tx_wr_data_out, fb_tx_rd_data_out : std_logic_vector(31 downto 0);
signal fb_tx_wr_wren, fb_tx_rd_wren : std_logic;
signal octet_cnt : unsigned(2 downto 0);
signal wr_ptr, rd_ptr : unsigned(9 downto 0);
signal fb_ready : std_logic;
signal frame_length : std_logic_vector(10 downto 0); -- without CRC
signal frame_length_cnt : unsigned(10 downto 0);
signal hold_tx_data : std_logic_vector(7 downto 0);

signal fb_wr_cur_frame, fb_rd_cur_frame : std_logic;

begin

dp_ram_rx : dp_ram 
    port map(
		address_a	=> fb_tx_rd_addr,
		address_b	=> fb_tx_wr_addr,
		clock_a	    => rmii_clk_i,
		clock_b	    => sys_clk_i,
		data_a	    => fb_tx_rd_data_in,
		data_b	    => fb_tx_wr_data_in,
		wren_a	    => fb_tx_rd_wren,
		wren_b	    => fb_tx_wr_wren,
		q_a	        => fb_tx_rd_data_out,
		q_b	        => fb_tx_wr_data_out
	);


----------------------------------------------------------
-- process: Output to MAC from framebuffer
----------------------------------------------------------
fb_tx_rd_addr <= fb_rd_cur_frame & std_logic_vector(rd_ptr(rd_ptr'length-2 downto 0));

--process(octet_cnt, fb_tx_rd_data_out)
--begin
--    case octet_cnt is
--        when "000" =>
--            octet_o <= fb_tx_rd_data_out(31 downto 24);
--        when "001" =>
--            octet_o <= fb_tx_rd_data_out(23 downto 16);
--        when "010" =>
--            octet_o <= fb_tx_rd_data_out(15 downto 8);
--        when "011" =>
--            octet_o <= fb_tx_rd_data_out(7 downto 0);
--        when others => 
--            octet_o <= x"00";
--    end case;
--end process;

fsm_tx_rd: process(all)
begin
    if (sys_rst = '1') then
        fsm_rd_state <= init;
        fb_rd_cur_frame <= '0';
        rd_ptr <= (others=>'0');
        ready_o <= '0';
        valid_o <= '0';
        fb_tx_rd_data_in <= (others=>'0');
        fb_tx_rd_wren <= '0';
        frame_length <= (others=>'0'); 
        frame_length_cnt <= (others=>'0'); 
        octet_cnt <= (others=>'0'); 
        eof_o <= '0';
        hold_tx_data <= (others=>'0');
        
    elsif(rising_edge(rmii_clk_i)) then
        fb_tx_rd_wren <= '0';
        
        case fsm_rd_state is
            when init =>
                eof_o <='0';
                fb_rd_cur_frame <= '0';
                fsm_rd_state <= poll_ready;
                rd_ptr <= (others=>'0');
                ready_o <= '0';

            when poll_ready =>
                frame_length_cnt <= (others=>'0');
                frame_length <= fb_tx_rd_data_out(10 downto 0);
                if (fb_tx_rd_data_out(31) = '1') then
                    fsm_rd_state <= octet_1;
                    ready_o <= '1';
                    rd_ptr <= rd_ptr + 1;
                end if;
                
            --when wait_start =>
            --    octet_cnt <= "000";
            --    if (start_i = '1') then
            --        frame_length_cnt <= frame_length_cnt + 1;
            --        valid_o <= '1';
            --        fsm_rd_state <= octet_transfer;
            --    end if;
                
            when octet_1 =>
                octet_o <= fb_tx_rd_data_out(31 downto 24);
                valid_o <= '1';
                if (octet_ack_i = '1') then
                    ready_o <= '0';
                    frame_length_cnt <= frame_length_cnt + 1;
                    octet_o <= fb_tx_rd_data_out(23 downto 16);
                    fsm_rd_state <= octet_2;
                end if;

                
            when octet_2 =>
                if (octet_ack_i = '1') then
                    frame_length_cnt <= frame_length_cnt + 1;
                    octet_o <= fb_tx_rd_data_out(15 downto 8);
                    fsm_rd_state <= octet_3;
                end if;
                
            when octet_3 =>
                --hold_tx_data <= fb_tx_rd_data_out(7 downto 0);
                if (octet_ack_i = '1') then
                    frame_length_cnt <= frame_length_cnt + 1;
                    octet_o <= fb_tx_rd_data_out(7 downto 0);
                    rd_ptr <= rd_ptr + 1;
                    --octet_o <= hold_tx_data;
                    fsm_rd_state <= octet_4;
                end if;
                
            when octet_4 =>
                if (octet_ack_i = '1') then
                    octet_o <= fb_tx_rd_data_out(31 downto 24);
                    frame_length_cnt <= frame_length_cnt + 1;
                    fsm_rd_state <= octet_1;
                    if (frame_length_cnt >= unsigned(frame_length)-1) then
                        fsm_rd_state <= free_buffer;
                        valid_o <= '0';
                        eof_o <= '1';
                    end if;
                end if;
                
                
            when octet_transfer =>
                if (octet_ack_i = '1') then
                    frame_length_cnt <= frame_length_cnt + 1;
                    if (octet_cnt = 3) then
                        rd_ptr <= rd_ptr + 1;
                    end if;
                    if (octet_cnt = 3) then
                        octet_cnt <= (others=>'0');
                    else
                        octet_cnt <= octet_cnt + 1;
                    end if;
                    -- delay, on first access:
                    --if (octet_cnt = 4) then
                    --    octet_cnt <= (others=>'0');
                    --end if;
                end if;
                if (frame_length_cnt >= unsigned(frame_length)) then
                    fsm_rd_state <= free_buffer;
                    valid_o <= '0';
                    eof_o <= '1';
                end if;

            when free_buffer =>
                eof_o <= '0';
                rd_ptr <= (others=>'0');
                fb_tx_rd_wren <= '1';
                fb_tx_rd_data_in <= x"0000_0000";  -- clear status bit
                fsm_rd_state <= waitstate;
                
            when waitstate =>
                    fsm_rd_state <= done;
                    
            when done =>
                fb_rd_cur_frame <= not fb_rd_cur_frame;
                fsm_rd_state <= poll_ready;
            
            when others =>
                fsm_rd_state <= done ;
        end case;
    end if;
end process;


----------------------------------------------------------
-- process: system writes to framebuffer
----------------------------------------------------------

fb_ready_o <= fb_ready;
fb_tx_wr_addr <= fb_wr_cur_frame & std_logic_vector(wr_ptr(wr_ptr'length-2 downto 0)) when fb_ready = '0' else 
                 fb_wr_cur_frame & fb_addr_i(fb_addr_i'length-2 downto 0);
fb_tx_wr_wren    <= fb_write_i;
fb_tx_wr_data_in <= fb_data_i;
                 

fsm_tx_wr: process(all)
begin
    if (sys_rst = '1') then
        fsm_wr_state <= init;
        fb_wr_cur_frame <= '0';
        wr_ptr <= (others=>'0');
        fb_ready <= '0';
        
    elsif(rising_edge(sys_clk_i)) then

       case fsm_wr_state is
            when init =>
                fb_wr_cur_frame <= '0';
                fsm_wr_state <= check_status;
                wr_ptr <= (others=>'0');
                fb_ready <= '0';
       
            when check_status =>
                wr_ptr <= "0000000000";
                fsm_wr_state <= check_status_wait;
            
            when check_status_wait =>
                if (fb_tx_wr_data_out(31) = '0') then
                    fsm_wr_state <= wait_done;
                    fb_ready <= '1';
                end if;
        
            when wait_done =>
                if (fb_done_i = '1') then
                    fb_ready <= '0';
                    fsm_wr_state <= done;
                end if;
    
            when done =>
                fb_wr_cur_frame <= not fb_wr_cur_frame;
                fsm_wr_state <= check_status;
                
        end case;
    end if;
    
end process;


end rtl;

