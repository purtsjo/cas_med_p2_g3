
library ieee;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std_unsigned.all;
use std.textio.all;
use ieee.std_logic_textio.all;

entity mac_tb is
-- testbench
end entity;

architecture tb of mac_tb is

component eth_mac is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- RMII interface
        rmii_clk_i    : in std_logic;
        rmii_tx_en_o  : out std_logic;
        rmii_tx_d_o   : out std_logic_vector(1 downto 0);
        rmii_rx_d_i   : in std_logic_vector(1 downto 0);
        rmii_rx_crs_i : in std_logic;
        -- system framebuffer interface
        fb_rx_addr_i  : in std_logic_vector(9 downto 0);
        fb_rx_data_o  : out std_logic_vector(31 downto 0);
        fb_rx_done_i  : in std_logic;
        fb_rx_ready_o : out std_logic;

        fb_tx_addr_i  : in std_logic_vector(9 downto 0);
        fb_tx_data_i  : in std_logic_vector(31 downto 0);
        fb_tx_write_i : in std_logic;
        fb_tx_done_i  : in std_logic;
        fb_tx_ready_o : out std_logic
    );
end component;

constant G_RMII_LOOPBACK : boolean := true;

-----------------------------------------------------------------------------
-- Signals:
-----------------------------------------------------------------------------
signal clk25, clk100, rmii_clk, rst_n, rst : std_logic;
signal rmii_tx_en, rmii_rx_crs : std_logic;
signal rmii_tx_d, rmii_rx_d : std_logic_vector(1 downto 0);
signal fb_rx_addr, fb_tx_addr : std_logic_vector(9 downto 0);
signal fb_rx_data, fb_tx_data : std_logic_vector(31 downto 0);
signal fb_rx_ready, fb_rx_done, fb_tx_ready, fb_tx_done, fb_tx_write : std_logic;


-----------------------------------------------------------------------------
-- Procedure:
-----------------------------------------------------------------------------
    procedure Avalon_MM_Write (constant addr : in std_logic_vector(31 downto 0);
                               constant data : in std_logic_vector(31 downto 0);
                               signal clk : in std_logic;
                               signal mm_addr : out std_logic_vector(31 downto 0);
                               signal mm_write : out std_logic;
                               --signal mm_read : out std_logic;
                               signal mm_writedata : out std_logic_vector(31 downto 0)
                               --signal mm_readdata : in std_logic_vector(31 downto 0)
                     ) is
    begin    
        --process
        --begin
            mm_addr <= (others=>'0');
            mm_write <= '0';
            mm_writedata <= (others=>'0');
            wait until clk = '1';
            mm_addr <= addr;
            mm_write <= '1';
            mm_writedata <= data;
            wait until clk = '0';
            mm_addr <= (others=>'0');
            mm_write <= '0';
            mm_writedata <= (others=>'0');
            wait until clk = '1';
        --end process;
  end procedure Avalon_MM_Write;

  
begin

eth_mac_inst : eth_mac
    --generic map(
    --)
    port map(
    -- CLK / RESET
        sys_clk_i    => clk100,
        sys_rst      => rst,
        -- RMII interface
        rmii_clk_i    => rmii_clk,
        rmii_tx_en_o  => rmii_tx_en,
        rmii_tx_d_o   => rmii_tx_d,
        rmii_rx_d_i   => rmii_rx_d,
        rmii_rx_crs_i => rmii_rx_crs,
        -- framebuffer RX
        fb_rx_addr_i  => fb_rx_addr,
        fb_rx_data_o  => fb_rx_data,
        fb_rx_done_i  => fb_rx_done,
        fb_rx_ready_o => fb_rx_ready,
        -- framebuffer TX
        fb_tx_addr_i  => fb_tx_addr,
        fb_tx_data_i  => fb_tx_data,
        fb_tx_write_i => fb_tx_write,
        fb_tx_done_i  => fb_tx_done,
        fb_tx_ready_o => fb_tx_ready
    );

    ----------------------------------------------------------
    -- Clock / Reset
    ----------------------------------------------------------
    -- clock 50 MHz:
    process
    begin
        rmii_clk <= '0';
        wait for 10 ns;
        rmii_clk <= '1';
        wait for 10 ns;
    end process;
    
    -- clock phy 25MHz:
    process
    begin
        clk25 <= '0';
        wait for 20 ns;
        clk25 <= '1';
        wait for 20 ns;
    end process;
    
    -- clock phy 100MHz:
    process
    begin
        clk100 <= '0';
        wait for 5 ns;
        clk100 <= '1';
        wait for 5 ns;
    end process;
    
    -- reset:
    process
    begin
        rst_n <= '0';
        wait for 1 us;
        rst_n <= '1';
        wait;
    end process;
    rst <= not rst_n;
    
    ----------------------------------------------------------
    -- RMII Ethernet frame generator
    ----------------------------------------------------------
gen_loopback_true: if G_RMII_LOOPBACK = true generate
        rmii_rx_d <= rmii_tx_d;
        rmii_rx_crs <= rmii_tx_en;
end generate gen_loopback_true;

gen_loopback_false: if G_RMII_LOOPBACK = false generate
    rmii_gen: process
        file frame_file : text is in "ethernetframe.dat";
        variable frame_line : line;
        variable frame_word : std_logic_vector(7 downto 0);
    begin
        rmii_rx_crs <= '0';
        rmii_rx_d <= "00";
        wait until rst_n = '1';
        wait for 100ns;
        wait until rmii_clk = '1';

        while not endfile(frame_file) loop
            wait until rmii_clk = '1';
            readline (frame_file, frame_line);
            hread (frame_line, frame_word);
            rmii_rx_crs <= frame_word(4);
            rmii_rx_d <= frame_word(1 downto 0);
        end loop;
        wait until rmii_clk = '1';
        rmii_rx_crs <= '0';
        rmii_rx_d <= "00";
    end process rmii_gen;
end generate gen_loopback_false;
    ----------------------------------------------------------
    -- Framebuffer RX readout
    ----------------------------------------------------------
    fb_rx_block: block
        
        type fsm_type is (init, wait_ready, frame_readout, done);
        
        signal fsm_state : fsm_type;
        signal fb_rx_addr_max : std_logic_vector(9 downto 0);
        
        signal write_en : std_logic;
    begin --block

    
    fb_rx: process(rst, clk100)
    begin

        if (rst = '1') then
            fsm_state <= init;
            fb_rx_done <= '0';
            fb_rx_addr <= (others=>'0');
            fb_rx_addr_max <= (others=>'1');
            write_en <= '0';
        elsif(rising_edge(clk100)) then
            fb_rx_done <= '0';
            write_en <= '0';
            
            if (write_en = '1') then
                write(OUTPUT, to_hstring(std_logic_vector((unsigned(fb_rx_addr) -1)))  & " : " & to_hstring(fb_rx_data) & "    (" & to_string(now) & ")" & LF);
            end if;
            
            case fsm_state is
                when init =>
                    fb_rx_done <= '0';
                    fb_rx_addr <= (others=>'0');
                    fsm_state <= wait_ready;
                    fb_rx_addr_max <= (others=>'1');
                    
                when wait_ready =>
                    fb_rx_addr <= (others=>'0');
                    if (fb_rx_ready = '1') then
                        fsm_state <= frame_readout;
                    end if;
            
                when frame_readout =>
                    if (fb_rx_addr = 0) then
                        fb_rx_addr_max <= fb_rx_data(25 downto 16);
                    end if;
                    fb_rx_addr <= fb_rx_addr + 1;
                    write_en <= '1';
                    
                    if (fb_rx_addr = fb_rx_addr_max) then
                        fsm_state <= done;
                        fb_rx_done <= '1';
                    end if;
                
                when done =>
                    fsm_state <= wait_ready;
                    fb_rx_addr_max <= (others=>'1');
                    write(OUTPUT, "============================================================" & LF);
                    
            end case;
            
        end if;
    end process fb_rx;    
end block;

fb_tx_block: block
    

begin    
    ----------------------------------------------------------
    -- Framebuffer TX write
    ----------------------------------------------------------
    tx_write: process
        file frame_file : text is in "payload.dat";
        variable frame_line : line;
        variable frame_word : std_logic_vector(35 downto 0);
        variable byte_cnt : std_logic_vector(10 downto 0);
    begin
        fb_tx_addr <= (others=>'0');
        fb_tx_data <= (others=>'0');
        fb_tx_done <= '0';
        fb_tx_write <= '0';
        wait until rst_n = '1';
        wait for 16us;
        fb_tx_addr <= "0000000000";
        wait until clk100 = '1';
        byte_cnt := (others=>'0');
        
        while true loop
            wait until clk100 = '1';
            readline (frame_file, frame_line);
            hread (frame_line, frame_word);
            if (frame_word(32) = '1') then
                byte_cnt := byte_cnt + 4;
            end if;
            fb_tx_write <= frame_word(32);
            fb_tx_data <= frame_word(31 downto 0);
            if (frame_word(32)) then
                fb_tx_addr <= fb_tx_addr + 1;
            end if;
            if (frame_word(33) = '1') then
                fb_tx_data <= (others=>'0');
                fb_tx_data(31) <= '1';
                --fb_tx_data(10 downto 0) <= byte_cnt;
                fb_tx_data(10 downto 0) <= frame_word(10 downto 0);
                fb_tx_addr <= (others=>'0');
                fb_tx_write <= '1';
            end if;
            if (frame_word(35) = '1') then
                report "TX Write completed";
                wait;
            end if;
        end loop;
        wait until clk100 = '1';
        fb_tx_done <= '1';
        fb_tx_write <= '0';
        wait until clk100 = '1';
        fb_tx_done <= '0';
        wait;
    end process;
    end block;


end architecture;


