vlib work

#vmap <external lib name> <external lib path>
   
set src_path ..

vcom -2008 $src_path/PCK_CRC32_D8.vhd
vcom -2008 $src_path/arch/dp_ram.vhd
vcom -2008 $src_path/framebuffer_rx.vhd
vcom -2008 $src_path/framebuffer_tx.vhd
vcom -2008 $src_path/eth_mac.vhd
vcom -2008 $src_path/sim/mac_tb.vhd

vsim -t ps mac_tb
set NumericStdNoWarnings 1
set StdArithNoWarnings 1

view wave

add wave -noupdate -divider "Signals"
add wave -noupdate -radix hex -color green /mac_tb/*
add wave -noupdate -radix hex -color green /mac_tb/fb_rx_block/*

add wave -noupdate -divider "ETHERNET MAC"
add wave -noupdate -radix hex -color cyan /mac_tb/eth_mac_inst/*

add wave -noupdate -divider "Framebuffer RX"
add wave -noupdate -radix hex -color orange /mac_tb/eth_mac_inst/fb_rx_inst/*

add wave -noupdate -divider "Framebuffer TX"
add wave -noupdate -radix hex -color yellow /mac_tb/eth_mac_inst/fb_tx_inst/*

run 30us
wave zoom full
