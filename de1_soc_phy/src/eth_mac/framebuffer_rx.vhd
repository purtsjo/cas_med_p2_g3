
----------------------------------------------------------------------
--
--
--
----------------------------------------------------------------------
--

library ieee;
use ieee.std_logic_1164.all;    
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
--USE ieee.numeric_std_unsigned.all;

entity framebuffer_rx is
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- MAC interface:
        rmii_clk_i : in std_logic;
        sof_i   : in std_logic;
        eof_i   : in std_logic;
        octet_i : in std_logic_vector(7 downto 0);
        octet_en_i : in std_logic;
        length_i : in std_logic_vector(10 downto 0);
        -- System interface:
        fb_addr_i : in std_logic_vector(9 downto 0);
        fb_data_o : out std_logic_vector(31 downto 0);
        fb_done_i : in std_logic;
        fb_ready_o : out std_logic
    );
end framebuffer_rx;

architecture rtl of framebuffer_rx is

component dp_ram
	PORT
	(
		address_a		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		address_b		: IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		clock_a		: IN STD_LOGIC  := '1';
		clock_b		: IN STD_LOGIC ;
		data_a		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		data_b		: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		wren_a		: IN STD_LOGIC  := '0';
		wren_b		: IN STD_LOGIC  := '0';
		q_a		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0);
		q_b		: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
end component;

type fsm_type is (init, check_status, check_status_wait, wait_sof, frame_active, write_length, done);
type fsm_rd_type is (init, poll_ready, frame_readout, free_buffer, done);

signal fsm_state : fsm_type;
signal fsm_rd_state : fsm_rd_type;

signal fb_rx_wr_addr, fb_rx_rd_addr : std_logic_vector(9 downto 0);
signal fb_rx_wr_data_in, fb_rx_rd_data_in, fb_rx_wr_data_out, fb_rx_rd_data_out : std_logic_vector(31 downto 0);
signal fb_rx_wr_wren, fb_rx_rd_wren : std_logic;
signal octet_cnt : unsigned(2 downto 0);
signal wr_ptr, rd_ptr : unsigned(9 downto 0);
signal fb_ready : std_logic;
signal frame_length : std_logic_vector(10 downto 0); -- without CRC
signal frame_length_words : unsigned(10 downto 0); -- without CRC

signal fb_wr_cur_frame, fb_rd_cur_frame : std_logic;

begin

dp_ram_rx : dp_ram 
    port map(
		address_a	=> fb_rx_wr_addr,
		address_b	=> fb_rx_rd_addr,
		clock_a	    => rmii_clk_i,
		clock_b	    => sys_clk_i,
		data_a	    => fb_rx_wr_data_in,
		data_b	    => fb_rx_rd_data_in,
		wren_a	    => fb_rx_wr_wren,
		wren_b	    => fb_rx_rd_wren,
		q_a	        => fb_rx_wr_data_out,
		q_b	        => fb_rx_rd_data_out
	);


----------------------------------------------------------
-- process: MAC write to framebuffer
----------------------------------------------------------
fb_rx_wr_addr <= fb_wr_cur_frame & std_logic_vector(wr_ptr(wr_ptr'length-2 downto 0));

fsm_rx_wr: process(all)
begin
    if (sys_rst = '1') then
        fsm_state <= init;
        wr_ptr <= (others=>'0');
        octet_cnt <= (others=>'0');
        fb_rx_wr_data_in <= (others=>'0');
        fb_rx_wr_wren <= '0';
        frame_length <= (others=>'0');
        fb_wr_cur_frame <= '0';
        frame_length_words <= (others=>'0');
        
    elsif(rising_edge(rmii_clk_i)) then
        fb_rx_wr_wren <= '0';

        case fsm_state is
            when init =>
                fb_wr_cur_frame <= '0';
                fsm_state <= check_status;
                frame_length_words <= (others=>'0');
                
            when check_status =>
                wr_ptr <= "0000000000";
                fsm_state <= check_status_wait;
            
            when check_status_wait =>
                if (fb_rx_wr_data_out(31)  = '0') then
                    fsm_state <= wait_sof;
                end if;
        
            when wait_sof =>
                octet_cnt <= (others=>'0');
                frame_length_words <= (others=>'0');
                if (sof_i = '1') then
                    wr_ptr <= "0000000000";
                    fsm_state <= frame_active;
                end if;

            when frame_active =>
                if (octet_en_i = '1') then
                    if (octet_cnt = 3) then
                        fb_rx_wr_wren <= '1';
                        frame_length_words <= frame_length_words + 1;
                        wr_ptr <= wr_ptr + 1;
                        octet_cnt <= (others=>'0');
                    else
                        octet_cnt <= octet_cnt + 1;
                    end if;
                    fb_rx_wr_data_in <= fb_rx_wr_data_in(23 downto 0) & octet_i;
                end if;
                -- wait for end of frame:
                if (eof_i = '1') then
                    frame_length <= std_logic_vector(unsigned(length_i) - 4);
                    fsm_state <= write_length;
                    wr_ptr <= "0000000000";
                end if;
                
            when write_length =>
                fb_rx_wr_data_in <= "10000" & std_logic_vector(frame_length_words) & "00000" & frame_length;  -- frame frame length and set ready bit (bit 16)
                fb_rx_wr_wren <= '1';
                fsm_state <= done;
            
            when done =>
                fb_wr_cur_frame <= not fb_wr_cur_frame;
                fsm_state <= check_status;
                
        end case;
    end if;
end process;


----------------------------------------------------------
-- process: system reads from framebuffer
----------------------------------------------------------
fb_ready_o <= fb_ready;
fb_rx_rd_addr <= fb_rd_cur_frame & std_logic_vector(rd_ptr(rd_ptr'length-2 downto 0)) when fb_ready = '0' else 
                 fb_rd_cur_frame & fb_addr_i(fb_addr_i'length-2 downto 0);
fb_data_o <= fb_rx_rd_data_out;

fsm_rx_rd: process(all)
begin
    if (sys_rst = '1') then
        fsm_rd_state <= init;
        fb_rd_cur_frame <= '0';
        rd_ptr <= (others=>'0');
        fb_ready <= '0';
        fb_rx_rd_data_in <= (others=>'0');
        fb_rx_rd_wren <= '0';
        
    elsif(rising_edge(sys_clk_i)) then
        fb_rx_rd_wren <= '0';
        
        case fsm_rd_state is
            when init =>
                fb_rd_cur_frame <= '0';
                fsm_rd_state <= poll_ready;
                rd_ptr <= (others=>'0');
                fb_ready <= '0';

            when poll_ready =>
                if (fb_rx_rd_data_out(31) = '1') then
                    fb_ready <= '1';
                    fsm_rd_state <= frame_readout;
                end if;
            
            when frame_readout =>
                if (fb_done_i = '1') then
                    fb_ready <= '0';
                    fsm_rd_state <= free_buffer;
                end if;
            
            when free_buffer =>
                rd_ptr <= (others=>'0');
                fb_rx_rd_wren <= '1';
                fb_rx_rd_data_in <= x"0000_0000";  -- clear status bit
                fsm_rd_state <= done;
                
            when done =>
                fb_rd_cur_frame <= not fb_rd_cur_frame;
                fsm_rd_state <= poll_ready;
            
        end case;
    end if;
end process;

end rtl;

