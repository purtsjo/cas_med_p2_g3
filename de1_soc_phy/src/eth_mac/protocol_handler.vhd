
----------------------------------------------------------------------
--
--
--
----------------------------------------------------------------------
--

library ieee;
use ieee.std_logic_1164.all;    
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;
USE work.PCK_CRC32_D8.all;
use std.textio.all;
use ieee.std_logic_textio.all;
--USE ieee.numeric_std_unsigned.all;

entity protocol_handler is
    generic(G_IP_ADDR         : std_logic_vector(31 downto 0) := x"C0_A8_00_46";
            G_MAC_ADDR        : std_logic_vector(47 downto 0) := x"00_33_44_55_66_01";
            G_TARGET_IP_ADDR  : std_logic_vector(31 downto 0) := x"C0_A8_00_47";
            G_TARGET_MAC_ADDR : std_logic_vector(47 downto 0) := x"00_33_44_55_66_02"
            );
    port(
        sys_clk_i     : in std_logic;
        sys_rst       : in std_logic;
        -- system interface to framebuffer:
        fb_rx_addr_o  : out std_logic_vector(9 downto 0);
        fb_rx_data_i  : in std_logic_vector(31 downto 0);
        fb_rx_done_o  : out std_logic;
        fb_rx_ready_i : in std_logic;

        fb_tx_addr_o  : out std_logic_vector(9 downto 0);
        fb_tx_data_o  : out std_logic_vector(31 downto 0);
        fb_tx_write_o : out std_logic;
        fb_tx_done_o  : out std_logic;
        fb_tx_ready_i : in std_logic;
        
        ram_addr_o  : out std_logic_vector(31 downto 0);
        ram_write_o : out std_logic;
        ram_data_o  : out std_logic_vector(31 downto 0);
        ram_data_i  : in std_logic_vector(31 downto 0);
        ram_access_o: out std_logic;
        
        loopback_val_o : out std_logic_vector(7 downto 0);
        loopback_err_o : out std_logic_vector(15 downto 0);
        
        
        wb_slv_adr_i   : in  std_logic_vector(31 downto 0);
        wb_slv_dat_i   : in  std_logic_vector(31 downto 0);
        wb_slv_dat_o   : out std_logic_vector(31 downto 0);
        wb_slv_sel_i   : in  std_logic_vector(3 downto 0);
        wb_slv_stb_i   : in  std_logic;
        wb_slv_cyc_i   : in  std_logic;
        wb_slv_we_i    : in  std_logic;
        wb_slv_ack_o   : out std_logic;
        wb_slv_err_o   : out std_logic;
        wb_slv_int_o   : out std_logic;
        wb_slv_stall_o : out std_logic
    );
end protocol_handler;

architecture rtl of protocol_handler is

type mac_type is
  record
     src_mac : std_logic_vector(47 downto 0);
     dst_mac : std_logic_vector(47 downto 0);
     ethertype : std_logic_vector(15 downto 0);
  end record;

type ip_type is
  record
     src_ip   : std_logic_vector(31 downto 0);
     dst_ip   : std_logic_vector(31 downto 0);
     src_port : std_logic_vector(15 downto 0);
     dst_port : std_logic_vector(15 downto 0);
     udp_length  : std_logic_vector(15 downto 0);
  end record;

type rx_state_type is ( init, wait_ready, read_length, read_mac_1, read_mac_2, read_mac_3, read_type,
                        check_ethertype, UDP_check_prot, UDP_read_header_1, UDP_read_header_2,
                        UDP_read_header_3, UDP_read_header_4, UDP_filter, ECHO_wait_tx_ready,
                        ECHO_write_header_0, ECHO_write_header_1, ECHO_write_header_2,
                        ECHO_write_header_3, ECHO_write_header_4, ECHO_write_header_5,
                        ECHO_write_header_6, ECHO_write_header_7, ECHO_write_header_8,
                        ECHO_write_header_9, ECHO_write_header_10, ECHO_write_header_11,
                        ECHO_write_header_12, ECHO_write_header_13, ECHO_write_finish,
                        ECHO_wb_access_0, ECHO_wb_access_1, ECHO_wb_access_2, ECHO_wb_access_3, ECHO_wb_access_4, ECHO_wb_access_5,
                        ECHO_wb_access_6, ECHO_write_done, wait_ack,
                        done, done2,dbg_wait_cnt);
 
type wb_state_type is (wait_stb, wait_eth_ack, wb_ack_end);
 
signal wb_fsm : wb_state_type;
signal eth_rx_mac : mac_type;
signal eth_rx_ip : ip_type;
signal reg0 : std_logic_vector(31 downto 0);
signal wb_hdr_access : std_logic_vector(7 downto 0);

signal wb_ack  : std_logic;
signal wb_data_in : std_logic_vector(31 downto 0);
signal wb_data_out : std_logic_vector(31 downto 0);
signal wb_write_en  : std_logic;
signal wb_addr : std_logic_vector(31 downto 0);
signal rx_length : std_logic_Vector(10 downto 0);
signal fsm_rx_state : rx_state_type;
signal checksum : unsigned(31 downto 0);
signal wait_cnt : unsigned(31 downto 0);
signal packet_loop : std_logic;
signal loopback_reg : std_logic_vector(7 downto 0);
signal loopback_error : unsigned(15 downto 0);
signal loopback_en    : std_logic;
signal eth_req : std_logic;
signal eth_ack : std_logic;

begin

    ram_addr_o  <= wb_addr;
    ram_data_o <= wb_data_out;
    wb_data_in <= ram_data_i;
    ram_write_o <= wb_write_en;
    
    loopback_val_o <= loopback_reg;
    loopback_err_o <= std_logic_vector(loopback_error);
--------------------------------------------
-- process: WB Slave
--------------------------------------------

wb_fsm_p: process(all)
variable ip_checksum  : unsigned(31 downto 0);
variable ip_checksum_cut  : unsigned(15 downto 0);
variable data_val : unsigned(15 downto 0);
begin
    if (sys_rst = '1') then
       wb_slv_dat_o    <= (others=>'0');
       wb_ack   <= '0';
       wb_slv_err_o   <= '0';
       wb_slv_int_o   <= '0';
       wb_slv_stall_o  <= '0';
       wb_fsm <= wait_stb;
       eth_req <= '0';
        
    elsif(rising_edge(sys_clk_i)) then
       wb_slv_err_o   <= '0';
       wb_slv_int_o   <= '0';
       wb_slv_stall_o  <= '0';
       
    case wb_fsm is 
        when wait_stb =>
            if (wb_slv_stb_i = '1' and wb_slv_cyc_i = '1') then
                eth_req <= '1';
                wb_fsm <= wait_eth_ack;
            end if;
        
        when wait_eth_ack =>
            if (eth_ack = '1') then
                eth_req <= '0';
                wb_ack <= '1';
                wb_fsm <= wb_ack_end;
            end if;
            
        when wb_ack_end =>
            if (wb_ack = '1' and wb_slv_stb_i = '0') then
                wb_ack <= '0';
                wb_fsm <= wait_stb;
            end if;
        end case;
       --wb_slv_dat_i   : in  std_logic_vector(31 downto 0);
    end if;

end process;
 
wb_slv_ack_o <= wb_ack;
--------------------------------------------
-- process: filter packet
--------------------------------------------
fsm_rx: process(all)
variable ip_checksum  : unsigned(31 downto 0);
variable ip_checksum_cut  : unsigned(15 downto 0);
variable data_val : unsigned(15 downto 0);
begin
    if (sys_rst = '1') then
        fsm_rx_state <= init;
        --fb_rx_addr_o <= (others=>'0');
        fb_rx_done_o <= '0';
        fb_tx_done_o <= '0';
        fb_tx_write_o <= '0';
        fb_tx_addr_o  <=(others=>'0');
        fb_tx_data_o  <=(others=>'0');
        wb_hdr_access  <=(others=>'0');
        packet_loop <= '0';
        wait_cnt <= (others=>'0');
        rx_length <= (others=>'0');
        eth_rx_mac <= (others=>(others=>'0'));
        eth_rx_ip <= (others=>(others=>'0'));
        --temp_data <= (others=>'0');
        reg0 <= (others=>'0');
        
        wb_data_out <= (others=>'0');
        wb_addr <= x"00000000";
        ram_access_o <= '0';
        wb_write_en <= '0';
        ip_checksum := (others=>'0');
        loopback_error <= (others=>'0');
        loopback_en <= '0';
        
    elsif(rising_edge(sys_clk_i)) then
        --fb_rx_addr_o <= "0000000000";
        fb_rx_done_o <= '0';
        fb_tx_done_o <= '0';
        fb_tx_write_o <= '0';
        eth_ack <= '0';
        
        case fsm_rx_state is
            when init =>
                fsm_rx_state <= wait_ready;
                
            when wait_ready =>
                if (eth_req = '1') then
                    fsm_rx_state <= ECHO_wait_tx_ready;
                end if;
            
                if (fb_rx_ready_i = '1') then
                    fsm_rx_state <= read_length;
                end if;
    
            when read_length =>
                rx_length <= fb_rx_data_i(10 downto 0);
                fsm_rx_state <= read_mac_1;
    
            when read_mac_1 =>
                eth_rx_mac.dst_mac(47 downto 16) <= fb_rx_data_i;
                fsm_rx_state <= read_mac_2;
                
            when read_mac_2 =>
                eth_rx_mac.dst_mac(15 downto 0) <= fb_rx_data_i(31 downto 16);
                eth_rx_mac.src_mac(47 downto 32) <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= read_mac_3;
                
            when read_mac_3 =>
                eth_rx_mac.src_mac(31 downto 0) <= fb_rx_data_i(31 downto 0);
                fsm_rx_state <= read_type;
                
            when read_type =>
                eth_rx_mac.ethertype <= fb_rx_data_i(31 downto 16);
                --temp_data <= fb_rx_data_i(31 downto 16);
                fsm_rx_state <= check_ethertype;
                
            when check_ethertype =>
                case eth_rx_mac.ethertype is
                    --when x"0806" => -- ARP
                    --    fsm_rx_state <= ARP_check_type;
                    when x"0800" => -- IP/UDP
                        fsm_rx_state <= UDP_check_prot;
                        
                    when others =>
                        fsm_rx_state <= done;
                end case;

            ------------------------------------------------------------
            -- ARP
            ------------------------------------------------------------
            --when ARP_check_type =>
            --    -- check if HW/IP size match:
            --    if (temp_data = x"0604") then
            --        
            --    end if;
            
            ------------------------------------------------------------
            -- UDP
            ------------------------------------------------------------
            when UDP_check_prot =>
                -- check IP protocol if UDP frame:
                if (fb_rx_data_i(7 downto 0) = x"11") then
                    fsm_rx_state <= UDP_read_header_1;
                else
                    fsm_rx_state <= done;
                end if;
                
            when UDP_read_header_1 =>
                eth_rx_ip.src_ip(31 downto 16) <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= UDP_read_header_2;

            when UDP_read_header_2 =>
                eth_rx_ip.src_ip(15 downto 0)  <= fb_rx_data_i(31 downto 16);
                eth_rx_ip.dst_ip(31 downto 16) <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= UDP_read_header_3;
                            
            when UDP_read_header_3 =>
                eth_rx_ip.dst_ip(15 downto 0) <= fb_rx_data_i(31 downto 16);
                eth_rx_ip.src_port            <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= UDP_read_header_4;

            when UDP_read_header_4 =>
                eth_rx_ip.dst_port <= fb_rx_data_i(31 downto 16);
                eth_rx_ip.udp_length <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= UDP_filter;                
            
            when UDP_filter =>
                if (eth_rx_ip.dst_ip = G_IP_ADDR) then
                    case eth_rx_ip.dst_port is
                        when x"0007" => -- ECHO
                            --fsm_rx_state <= ECHO_wait_tx_ready;
                            fsm_rx_state <= done;
                        when x"0020" => -- 32
                            fsm_rx_state <= ECHO_wb_access_0;
                            -- wishbone
                        when others =>
                            fsm_rx_state <= done;
                    end case;
                else
                    fsm_rx_state <= done;
                end if;
            --------------------------------------------------------
            when ECHO_wb_access_0 => 
                fsm_rx_state <= ECHO_wb_access_1;
                
            when ECHO_wb_access_1 => 
                loopback_en <= '0';
                wb_hdr_access <= fb_rx_data_i(7 downto 0);
                if (fb_rx_data_i(15 downto 12) = "0000") then  -- check if packt is a "request"
                    case fb_rx_data_i(3 downto 0) is
                        when "0000" =>
                            ram_access_o <= '0';
                            fsm_rx_state <= ECHO_wait_tx_ready;
                        when "0100" => -- enable/disable access mode
                            ram_access_o <= '1';
                            fsm_rx_state <= ECHO_wait_tx_ready;
                        when "1000" => -- loopback ramp counter
                            loopback_en <= '1';
                            fsm_rx_state <= ECHO_wb_access_2;
                        when "0010" => --write
                            fsm_rx_state <= ECHO_wb_access_2;
                        when "0001" => -- read
                            fsm_rx_state <= ECHO_wait_tx_ready;
                        when others =>
                            fsm_rx_state <= ECHO_wait_tx_ready;
                    end case;
                else
                    fsm_rx_state <= done;
                end if;
                
            when ECHO_wb_access_2 => 
                wb_addr(31 downto 16) <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= ECHO_wb_access_3;
                
            when ECHO_wb_access_3 =>
                wb_addr(15 downto 0)      <= fb_rx_data_i(31 downto 16);
                wb_data_out(31 downto 16) <= fb_rx_data_i(15 downto 0);
                fsm_rx_state <= ECHO_wb_access_4;
              
            when ECHO_wb_access_4 =>
                wb_data_out(15 downto 0) <= fb_rx_data_i(31 downto 16);
                fsm_rx_state <= ECHO_wb_access_5;
              
            when ECHO_wb_access_5 =>
                wb_write_en <= '1';
                fsm_rx_state <= ECHO_wb_access_6;
                
            when ECHO_wb_access_6 =>
                wb_write_en <= '0';
                fsm_rx_state <= ECHO_wait_tx_ready;

            --------------------------------------------------------
            when ECHO_wait_tx_ready =>
                if (fb_tx_ready_i = '1') then
                    fsm_rx_state <= ECHO_write_header_0;
                end if;
            
            when ECHO_write_header_0 =>
                -- if ramp loopback enabled check ramp:
                if (loopback_en = '1') then
                    if (wb_addr(7 downto 0) = x"00") then
                        loopback_error <= loopback_error + 1;
                    elsif (wb_addr(7 downto 0) /= x"01") then
                        if (unsigned(wb_addr(7 downto 0)) /= unsigned(loopback_reg) + 1 ) then
                            loopback_error <= loopback_error + 1;                   
                        end if;
                    end if;
                    loopback_reg <= wb_addr(7 downto 0);
                end if;
                ip_checksum := (others=>'0');
                fb_tx_addr_o <= b"00_0000_0001";
                if (eth_req = '1') then
                    fb_tx_data_o <= G_TARGET_MAC_ADDR(47 downto 16);
                else
                    fb_tx_data_o <= eth_rx_mac.src_mac(47 downto 16);
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_1;
                    
            when ECHO_write_header_1 =>
                fb_tx_addr_o <= b"00_0000_0010";
                if (eth_req = '1') then
                    fb_tx_data_o(31 downto 16) <= G_TARGET_MAC_ADDR(15 downto 0);
                    fb_tx_data_o(15 downto 0)  <= G_MAC_ADDR(47 downto 32);
                else
                    fb_tx_data_o(31 downto 16) <= eth_rx_mac.src_mac(15 downto 0);
                    fb_tx_data_o(15 downto 0) <= eth_rx_mac.dst_mac(47 downto 32);
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_2;
                
            when ECHO_write_header_2 =>
                fb_tx_addr_o <= b"00_0000_0011";
                if (eth_req = '1') then
                    fb_tx_data_o <= G_MAC_ADDR(31 downto 0);
                else
                    fb_tx_data_o <= eth_rx_mac.dst_mac(31 downto 0);
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_3;  
                
            when ECHO_write_header_3 =>
                fb_tx_addr_o <= b"00_0000_0100";
                fb_tx_data_o(31 downto 16) <= eth_rx_mac.ethertype;
                data_val :=  x"4500"; -- version / hdr len / diff. serv.
                fb_tx_data_o(15 downto 0) <= std_logic_vector(data_val);
                ip_checksum := ip_checksum + data_val;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_4;  
                
            when ECHO_write_header_4 =>
                fb_tx_addr_o <= b"00_0000_0101";
                data_val := x"0028"; -- total length in bytes
                fb_tx_data_o(31 downto 16) <=  std_logic_vector(data_val);
                ip_checksum := ip_checksum + data_val;
                data_val := x"0000"; --   -- identification (f�r reassembly);
                fb_tx_data_o(15 downto 0) <=  std_logic_vector(data_val);
                ip_checksum := ip_checksum + data_val;
                 fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_5;  
                
            when ECHO_write_header_5 =>
                fb_tx_addr_o <= b"00_0000_0110";
                data_val := "010" & "0000000000000";  -- flags: dont fragment / fragment offset
                fb_tx_data_o(31 downto 16) <= std_logic_vector(data_val);  
                ip_checksum := ip_checksum + data_val;
                data_val := x"8011";  -- flags: dont fragment / fragment offset
                fb_tx_data_o(15 downto 0)  <= std_logic_vector(data_val);  -- TTL, Prot
                ip_checksum := ip_checksum + data_val;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_7;  
                
            when ECHO_write_header_6 =>
                fb_tx_addr_o <= b"00_0000_0111";
                data_val := unsigned(G_IP_ADDR(31 downto 16));
                fb_tx_data_o(15 downto 0)  <= std_logic_vector(data_val);
                ip_checksum := ip_checksum + data_val;
                ip_checksum_cut := ip_checksum(15 downto 0) + ip_checksum(31 downto 16);
                ip_checksum_cut := not ip_checksum_cut;
                fb_tx_data_o(31 downto 16) <= std_logic_vector(ip_checksum_cut);  -- header checksum
                --checksum <= x"0000" & not ip_checksum_cut;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_9;  
                
            when ECHO_write_header_7 =>
                fb_tx_addr_o <= b"00_0000_1000";
                data_val := unsigned(G_IP_ADDR(15 downto 0));
                ip_checksum := ip_checksum + data_val;
                fb_tx_data_o(31 downto 16) <= std_logic_vector(data_val);
                if (eth_req = '1') then
                    data_val := unsigned(G_TARGET_IP_ADDR(31 downto 16));
                else
                    data_val := unsigned(eth_rx_ip.src_ip(31 downto 16));
                end if;
                fb_tx_data_o(15 downto 0)  <= std_logic_vector(data_val);
                ip_checksum := ip_checksum + data_val;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_8;
                
            when ECHO_write_header_8 =>
                fb_tx_addr_o <= b"00_0000_1001";
                if (eth_req = '1') then
                    data_val := unsigned(G_TARGET_IP_ADDR(15 downto 0));
                else
                    data_val := unsigned(eth_rx_ip.src_ip(15 downto 0));
                end if;
                fb_tx_data_o(31 downto 16) <= std_logic_vector(data_val); 
                ip_checksum := ip_checksum + data_val;
                if (eth_req = '1') then
                    fb_tx_data_o(15 downto 0)  <= x"0030"; --src port
                else
                    fb_tx_data_o(15 downto 0)  <= x"0020"; --src port
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_6;  
                
            when ECHO_write_header_9 =>
                fb_tx_addr_o <= b"00_0000_1010";
                if (eth_req = '1') then
                    fb_tx_data_o(31 downto 16) <= x"0020";  -- dst port
                else
                    fb_tx_data_o(31 downto 16) <= eth_rx_ip.src_port;  -- dst port
                end if;
                fb_tx_data_o(15 downto 0)  <= x"0014"; -- UDP length in bytes
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_10;  

            when ECHO_write_header_10 =>
                fb_tx_addr_o <= b"00_0000_1011";
                fb_tx_data_o(31 downto 16) <= x"0000";  -- checksum (disable)
                if (eth_req = '1') then
                    fb_tx_data_o(15 downto 0)  <= x"01" & wb_hdr_access; -- WB Header: Request
                else
                    fb_tx_data_o(15 downto 0)  <= x"10" & wb_hdr_access; -- WB Header: Reply
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_11;  
                
            when ECHO_write_header_11 =>
                fb_tx_addr_o <= b"00_0000_1100";
                fb_tx_data_o(31 downto 16) <= x"0008";  -- WB Header: length in bytes
                if (loopback_en = '1') then
                    fb_tx_data_o(15 downto 0) <= (others=>'0');
                else
                    if (eth_req = '1') then
                        fb_tx_data_o(15 downto 0)  <= wb_slv_adr_i(31 downto 16);
                    else
                        fb_tx_data_o(15 downto 0)  <= wb_addr(31 downto 16);
                    end if;
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_12;        
                
            when ECHO_write_header_12 =>
                fb_tx_addr_o <= b"00_0000_1101";
                if (loopback_en = '1') then
                    fb_tx_data_o(31 downto 16) <= x"00" & loopback_reg;
                    fb_tx_data_o(15 downto 0) <= (others=>'0');
                else
                    if (eth_req = '1') then
                        fb_tx_data_o(31 downto 16) <= wb_slv_adr_i(15 downto 0); 
                        fb_tx_data_o(15 downto 0)  <= wb_slv_dat_i(31 downto 16);
                    else
                        fb_tx_data_o(31 downto 16) <= wb_addr(15 downto 0); 
                        fb_tx_data_o(15 downto 0)  <= wb_data_in(31 downto 16);
                    end if;
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_header_13;
                
            when ECHO_write_header_13 =>
                fb_tx_addr_o <= b"00_0000_1110";
                if (loopback_en = '1') then
                    fb_tx_data_o(31 downto 16) <= std_logic_vector(loopback_error);
                    fb_tx_data_o(15 downto 0) <= (others=>'0');
                else
                    if (eth_req = '1') then
                        fb_tx_data_o(31 downto 16) <= wb_slv_dat_i(15 downto 0); 
                        fb_tx_data_o(15 downto 0)  <= x"0000";
                    else
                        fb_tx_data_o(31 downto 16) <= wb_data_in(15 downto 0); 
                        fb_tx_data_o(15 downto 0)  <= x"0000";
                    end if;
                end if;
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_finish;     
                
            when ECHO_write_finish =>
                fb_tx_addr_o <= b"00_0000_0000";
                fb_tx_data_o <= x"80000036"; -- length, set ready flag 
                --fb_tx_data_o <= x"80000040"; -- length, set ready flag 
                fb_tx_write_o <= '1';
                fsm_rx_state <= ECHO_write_done;     

            when ECHO_write_done =>
                fb_tx_done_o <= '1';
                fsm_rx_state <= done;
                
            when done =>
                fb_rx_done_o <= '1';
                fsm_rx_state <= done2;
                
            when done2 =>
                if (eth_req = '1') then
                    fsm_rx_state <= wait_ack;
                else
                    fsm_rx_state <= wait_ready;
                end if;
                wait_cnt <= (others=>'0');
                
            when wait_ack =>
                eth_ack <= '1';
                if (eth_req = '0') then
                    eth_ack <= '0';
                    fsm_rx_state <= wait_ready;
                end if;
                
            when dbg_wait_cnt =>
                if (wait_cnt < 200000) then
                    wait_cnt <= wait_cnt + 1;
                else
                    fsm_rx_state <= ECHO_wait_tx_ready;
                end if;
            end case;
    end if;
end process;

-------------------------------------------------------------------------------------
-- framebuffer address transitions:
-------------------------------------------------------------------------------------
process(all)
begin
        fb_rx_addr_o <= b"00_0000_0000"; 

        case fsm_rx_state is
            when init =>
                
            when wait_ready =>
                fb_rx_addr_o <= "0000000000";
    
            when read_length =>
                fb_rx_addr_o <= b"00_0000_0001";
    
            when read_mac_1 =>
                fb_rx_addr_o <= b"00_0000_0010";
                
            when read_mac_2 =>
                fb_rx_addr_o <= b"00_0000_0011";
                
            when read_mac_3 =>
                fb_rx_addr_o <= b"00_0000_0100";
                
            when read_type =>
                fb_rx_addr_o <= b"00_0000_0101";
                
            when check_ethertype =>
                case eth_rx_mac.ethertype is
                    --when x"0806" => -- ARP
                    --    fsm_rx_state <= ARP_check_type;
                    when x"0800" => -- IP/UDP
                        fb_rx_addr_o <= b"00_0000_0110";
                        
                    when others =>

                end case;

            when UDP_check_prot =>
                -- check IP protocol if UDP frame:
                if (fb_rx_data_i(7 downto 0) = x"11") then
                    fb_rx_addr_o <= b"00_0000_0111";
                end if;

            when UDP_read_header_1 =>
                fb_rx_addr_o <= b"00_0000_1000";

            when UDP_read_header_2 =>
                fb_rx_addr_o <= b"00_0000_1001";
                            
            when UDP_read_header_3 =>
                fb_rx_addr_o <= b"00_0000_1010";

            when UDP_read_header_4 =>
                fb_rx_addr_o <= b"00_0000_1011";           

            when ECHO_wb_access_0 => 
                fb_rx_addr_o <= b"00_0000_1011";           
                
            when ECHO_wb_access_1 => 
                fb_rx_addr_o <= b"00_0000_1100";           

            when ECHO_wb_access_2 => 
                fb_rx_addr_o <= b"00_0000_1101";

            when ECHO_wb_access_3 =>
                fb_rx_addr_o <= b"00_0000_1110";
                
            when ECHO_wb_access_4 =>
              
            when ECHO_wb_access_5 =>
                
            when ECHO_wb_access_6 =>
            
            when others =>
                fb_rx_addr_o <= b"00_0000_0000";           
                
        end case;        
end process;


end architecture;


