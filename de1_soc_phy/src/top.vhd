
library ieee;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
USE ieee.std_logic_unsigned.all;


entity top is
  port(
    clk50         : in    std_logic;
    key0          : in    std_logic;
    -- Ethernet
    eth_clk       : in    std_logic;                      -- gpio_0_D0
    eth_tx_en     : out   std_logic;                      -- gpio_0_D3
    eth_tx_d      : out   std_logic_vector(1 downto 0);   -- 0=gpio_0_D1, 1=gpio_0_D5
    eth_rx_d      : in    std_logic_vector(1 downto 0);   -- 0=gpio_0_D7, 1=gpio_0_D4
    eth_rx_crs    : in    std_logic;                      -- gpio_0_D6
    eth_mdc       : out   std_logic;                      -- gpio_0_D8
    eth_mdio      : inout std_logic;                      -- gpio_0_D9
    -- UART
    uart_rx       : in    std_logic;
    uart_tx       : out   std_logic;
    ledr          : out   std_logic_vector(9 downto 0);
    -- 7-Segement
    hex0          : out   std_logic_vector(6 downto 0);
    hex1          : out   std_logic_vector(6 downto 0);
    hex2          : out   std_logic_vector(6 downto 0);
    hex3          : out   std_logic_vector(6 downto 0);
    hex4          : out   std_logic_vector(6 downto 0);
    hex5          : out   std_logic_vector(6 downto 0);
    -- SPI
    adc_cs_n      : out   std_logic;
    adc_dout      : in    std_logic;
    adc_din       : out   std_logic;
    adc_sclk      : out   std_logic
  );
end entity;


architecture rtl of top is

  -- ------------------------------------------------------
  -- Constant Declarations
  -- ------------------------------------------------------
  constant C_0 : std_logic_vector(6 downto 0) := "1000000";
  constant C_1 : std_logic_vector(6 downto 0) := "1111001";
  constant C_2 : std_logic_vector(6 downto 0) := "0100100";
  constant C_3 : std_logic_vector(6 downto 0) := "0110000";
  constant C_4 : std_logic_vector(6 downto 0) := "0011001";
  constant C_5 : std_logic_vector(6 downto 0) := "0010010";
  constant C_6 : std_logic_vector(6 downto 0) := "0000011";
  constant C_7 : std_logic_vector(6 downto 0) := "1111000";
  constant C_8 : std_logic_vector(6 downto 0) := "0000000";
  constant C_9 : std_logic_vector(6 downto 0) := "0011000";
  constant C_x : std_logic_vector(6 downto 0) := "1111111";

  -- ------------------------------------------------------
  -- Component Declarations
  -- ------------------------------------------------------
  component pll
  port (
    refclk   : in  std_logic := '0'; -- refclk.clk
    rst      : in  std_logic := '0'; -- reset.reset
    outclk_0 : out std_logic;        -- outclk0.clk
    locked   : out std_logic         -- locked.export
  );
  end component;

component pll_eth
	port (
		refclk   : in  std_logic := '0'; --  refclk.clk
		rst      : in  std_logic := '0'; --  reset.reset
		outclk_0 : out std_logic;        --  outclk0.clk
		locked   : out std_logic         --  locked.export
	);
end component;

  
  component eth_mac
    port(
        sys_clk_i     : in  std_logic;
        sys_rst       : in  std_logic;
        -- RMII interface
        rmii_rx_clk_i : in  std_logic;
		    rmii_tx_clk_i : in  std_logic;
        rmii_tx_en_o  : out std_logic;
        rmii_tx_d_o   : out std_logic_vector(1 downto 0);
        rmii_rx_d_i   : in  std_logic_vector(1 downto 0);
        rmii_rx_crs_i : in  std_logic;
        -- system interface to framebuffer:
        fb_rx_addr_i  : in  std_logic_vector(9 downto 0);
        fb_rx_data_o  : out std_logic_vector(31 downto 0);
        fb_rx_done_i  : in  std_logic;
        fb_rx_ready_o : out std_logic;

        fb_tx_addr_i  : in  std_logic_vector(9 downto 0);
        fb_tx_data_i  : in  std_logic_vector(31 downto 0);
        fb_tx_write_i : in  std_logic;
        fb_tx_done_i  : in  std_logic;
        fb_tx_ready_o : out std_logic
    );
  end component;

  component protocol_handler
    generic(
      G_IP_ADDR  : std_logic_vector(31 downto 0) := x"C0_A8_01_46";
      G_MAC_ADDR : std_logic_vector(47 downto 0) := x"00_33_44_55_66_01";
      G_TARGET_IP_ADDR  : std_logic_vector(31 downto 0) := x"C0_A8_01_11";
      G_TARGET_MAC_ADDR : std_logic_vector(47 downto 0) := x"C0_56_27_90_16_2A"
    );
    port(
      sys_clk_i     : in  std_logic;
      sys_rst       : in  std_logic;
      -- system interface to framebuffer:
      fb_rx_addr_o  : out std_logic_vector( 9 downto 0);
      fb_rx_data_i  : in  std_logic_vector(31 downto 0);
      fb_rx_done_o  : out std_logic;
      fb_rx_ready_i : in  std_logic;

      fb_tx_addr_o  : out std_logic_vector( 9 downto 0);
      fb_tx_data_o  : out std_logic_vector(31 downto 0);
      fb_tx_write_o : out std_logic;
      fb_tx_done_o  : out std_logic;
      fb_tx_ready_i : in  std_logic;

      ram_addr_o    : out std_logic_vector(31 downto 0);
      ram_write_o   : out std_logic;
      ram_data_o    : out std_logic_vector(31 downto 0);
      ram_data_i    : in  std_logic_vector(31 downto 0);
      ram_access_o  : out std_logic;
      
      loopback_val_o : out std_logic_vector(7 downto 0);
      loopback_err_o : out std_logic_vector(15 downto 0);
      
      wb_slv_adr_i   : in  std_logic_vector(31 downto 0);
      wb_slv_dat_i   : in  std_logic_vector(31 downto 0);
      wb_slv_dat_o   : out std_logic_vector(31 downto 0);
      wb_slv_stb_i   : in  std_logic;
      wb_slv_cyc_i   : in  std_logic;
      wb_slv_we_i    : in  std_logic;
      wb_slv_ack_o   : out std_logic
--      wb_slv_sel_i   : in  std_logic_vector(3 downto 0);
--      wb_slv_err_o   : out std_logic;
--      wb_slv_int_o   : out std_logic;
--      wb_slv_stall_o : out std_logic
        
    );
  end component;

  component urv_system
    generic (
      g_riscv_firmware : string  := "hello.mif";
      g_riscv_mem_size : integer := 65536;
      g_simulation     : boolean := false
    );
    port (
      rst_sys_i        : in  std_logic;
      rst_cpu_i        : in  std_logic;
      clk_sys_i        : in  std_logic;  -- 100 MHz
      uart_txd_o       : out std_logic;
      uart_rxd_i       : in  std_logic;
      gpio_out_o       : out std_logic_vector(31 downto 0);
      spi_cs_o         : out std_logic_vector( 7 downto 0);
      spi_sclk_o       : out std_logic;
      spi_mosi_o       : out std_logic;
      spi_miso_i       : in  std_logic;
      dm_data_eth_i    : in  std_logic_vector(31 downto 0);
      dm_data_eth_o    : out std_logic_vector(31 downto 0);
      dm_addr_eth_i    : in  std_logic_vector(31 downto 0);
      dm_we_eth_i      : in  std_logic;
      eth_control_i    : in  std_logic_vector(31 downto 0);
      eth_counter_i    : in  std_logic_vector(31 downto 0);
      eth_error_i      : in  std_logic_vector(31 downto 0);
      eth_buffer_o     : out std_logic_vector(31 downto 0);
      eth_data_rdy_o   : out std_logic
    );
  end component;

  -- ------------------------------------------------------
  -- Type Declarations
  -- ------------------------------------------------------
  type digit_type is array(1 to 6) of std_logic_vector(3 downto 0);
  type hex_type   is array(1 to 6) of std_logic_vector(6 downto 0);

  -- ------------------------------------------------------
  -- Signal Declarations
  -- ------------------------------------------------------
  signal clk100         : std_logic;
  signal pll_locked     : std_logic;
  signal rst_sys        : std_logic;
  signal pll_eth_locked : std_logic;
  signal eth_clk_out    : std_logic;
  
  -- peripheral
  signal uart_txd       : std_logic;
  signal gpio_out       : std_logic_vector(31 downto 0);
  signal digit_bin      : digit_type;
  signal hex            : hex_type;
  signal counter_rx     : std_logic_vector(23 downto 0);
  signal counter_tx     : std_logic_vector(23 downto 0);

  -- RAM
  signal ram_access     : std_logic;
  signal ram_data_in    : std_logic_vector(31 downto 0);
  signal ram_data_out   : std_logic_vector(31 downto 0);
  signal ram_addr       : std_logic_vector(31 downto 0);
  signal ram_write_en   : std_logic;

  -- Framebuffer
  signal fb_rx_addr     : std_logic_vector( 9 downto 0);
  signal fb_rx_data     : std_logic_vector(31 downto 0);
  signal fb_rx_done     : std_logic;
  signal fb_rx_ready    : std_logic;

  signal fb_tx_addr     : std_logic_vector( 9 downto 0);
  signal fb_tx_data     : std_logic_vector(31 downto 0);
  signal fb_tx_write    : std_logic;
  signal fb_tx_done     : std_logic;
  signal fb_tx_ready    : std_logic;
  
  signal  loopback_val  :  std_logic_vector(7 downto 0);
  signal loopback_err   : std_logic_vector(15 downto 0);
  
  signal prot_wb_adr, prot_wb_dat_in, prot_wb_dat_out : std_logic_vector(31 downto 0);
  signal prot_wb_stb, prot_wb_cyc,  prot_wb_we,  prot_wb_ack  : std_logic;
  
  signal eth_buffer : std_logic_vector(31 downto 0);
  signal eth_data_rdy : std_logic;
  

-- --------------------------------------------------------
-- --------------------------------------------------------
-- Main Body
-- --------------------------------------------------------
-- --------------------------------------------------------
begin

  inst_pll : pll port map(clk50, '0', clk100, pll_locked);
  inst_pll_eth : pll_eth port map(eth_clk, '0', eth_clk_out, pll_eth_locked);
  rst_sys <= not pll_locked or not key0;


  inst_urv: urv_system
    generic map(
      g_riscv_firmware  => "wb_devices.mif",
      g_riscv_mem_size  => 65536,
      g_simulation      => false
    )
    port map(
      rst_sys_i            => rst_sys,
      rst_cpu_i            => ram_access,
      clk_sys_i            => clk100,
      uart_txd_o           => uart_txd,
      uart_rxd_i           => uart_rx,
      gpio_out_o           => gpio_out,
      spi_cs_o(0)          => adc_cs_n,
      spi_cs_o(7 downto 1) => open,
      spi_sclk_o           => adc_sclk,
      spi_mosi_o           => adc_din,
      spi_miso_i           => adc_dout,
      dm_data_eth_i        => ram_data_out,
      dm_data_eth_o        => ram_data_in,
      dm_addr_eth_i        => ram_addr,
      dm_we_eth_i          => ram_write_en,
      eth_control_i        => X"0000_0000",
      eth_counter_i        => X"000000" & loopback_val,
      eth_error_i          => X"0000" & loopback_err,
      eth_buffer_o         => eth_buffer,
      eth_data_rdy_o       => eth_data_rdy
    );

  eth_mac_inst : eth_mac
    port map(
      -- CLK / RESET
      sys_clk_i     => clk100,
      sys_rst       => rst_sys,
      -- RMII interface
      rmii_rx_clk_i => eth_clk,
		  rmii_tx_clk_i => eth_clk_out,
      rmii_tx_en_o  => eth_tx_en,
      rmii_tx_d_o   => eth_tx_d,
      rmii_rx_d_i   => eth_rx_d,
      rmii_rx_crs_i => eth_rx_crs,
      -- framebuffer RX
      fb_rx_addr_i  => fb_rx_addr,
      fb_rx_data_o  => fb_rx_data,
      fb_rx_done_i  => fb_rx_done,
      fb_rx_ready_o => fb_rx_ready,
      -- framebuffer TX
      fb_tx_addr_i  => fb_tx_addr,
      fb_tx_data_i  => fb_tx_data,
      fb_tx_write_i => fb_tx_write,
      fb_tx_done_i  => fb_tx_done,
      fb_tx_ready_o => fb_tx_ready
    );

  prot : protocol_handler
    port map(
      -- CLK / RESET
      sys_clk_i     => clk100,
      sys_rst       => rst_sys,
      -- framebuffer RX
      fb_rx_addr_o  => fb_rx_addr,
      fb_rx_data_i  => fb_rx_data,
      fb_rx_done_o  => fb_rx_done,
      fb_rx_ready_i => fb_rx_ready,
      -- framebuffer TX
      fb_tx_addr_o  => fb_tx_addr,
      fb_tx_data_o  => fb_tx_data,
      fb_tx_write_o => fb_tx_write,
      fb_tx_done_o  => fb_tx_done,
      fb_tx_ready_i => fb_tx_ready,

      ram_addr_o    => ram_addr,
      ram_write_o   => ram_write_en,
      ram_data_o    => ram_data_out,
      ram_data_i    => ram_data_in,
      ram_access_o  => ram_access,
      
      loopback_val_o => loopback_val,
      loopback_err_o => loopback_err,
      
      wb_slv_adr_i   => prot_wb_adr,
      wb_slv_dat_i   => prot_wb_dat_in,
      wb_slv_dat_o   => prot_wb_dat_out,
      wb_slv_stb_i   => prot_wb_stb,
      wb_slv_cyc_i   => prot_wb_cyc,
      wb_slv_we_i    => prot_wb_we,
      wb_slv_ack_o   => prot_wb_ack
    );
    
    prot_wb_block : block
        type fsm_type is (wait_rdy, wb_req, wait_ack);
        signal prot_wb_state : fsm_type;
    begin
	 process(all)
	 begin
        if (rst_sys = '1') then
            prot_wb_adr    <= x"00000000";
            prot_wb_dat_in <= x"00000000";
            prot_wb_stb    <= '0';
            prot_wb_cyc    <= '0';
            prot_wb_we     <= '0';
            prot_wb_state  <= wait_rdy;
            
        elsif (rising_edge(clk100)) then
            case prot_wb_state is
                when wait_rdy =>
                    if (eth_data_rdy = '1') then
                        prot_wb_state <= wb_req;
                    end if;
                
                when wb_req =>
                    prot_wb_adr      <= x"0000000A";
                    prot_wb_dat_in   <= eth_buffer;
                    prot_wb_stb      <= '1';
                    prot_wb_cyc      <= '1';
                    prot_wb_we       <= '1';
                    prot_wb_state <= wait_ack;
                    
                when wait_ack =>
                    if (prot_wb_ack = '1') then
                        prot_wb_adr    <= x"00000000";
                        prot_wb_dat_in <= x"00000000";  -- eth_buffer
                        prot_wb_stb    <= '0';
                        prot_wb_cyc    <= '0';
                        prot_wb_we     <= '0';
                        prot_wb_state  <= wait_rdy;
                    end if;
            end case;
        end if;
    end process; 
    end block;

  extend_pulses: process(clk100, rst_sys)
  begin
    if rising_edge(clk100) then
     if rst_sys = '1' then
      counter_tx <= X"CC4B40";
      counter_rx <= X"CC4B40";
    else
      if uart_txd = '0' then
        counter_tx <= X"CC4B40";
      elsif counter_tx(counter_tx'high) = '1' then
        counter_tx <= counter_tx - 1;
      end if;
      if uart_rx = '0' then
        counter_rx <= X"CC4B40";
      elsif counter_rx(counter_rx'high) = '1' then
        counter_rx <= counter_rx - 1;
      end if;
      end if;
    end if;
  end process;


  -- port mapping
  uart_tx <= uart_txd;
  ledr(8) <= counter_tx(counter_tx'high);
  ledr(9) <= counter_rx(counter_rx'high);
  ledr(7 downto 0) <= gpio_out(31 downto 24);

  digit_bin(1) <= gpio_out( 3 downto 0 );
  digit_bin(2) <= gpio_out( 7 downto 4 );
  digit_bin(3) <= gpio_out(11 downto 8 );
  digit_bin(4) <= gpio_out(15 downto 12);
  digit_bin(5) <= gpio_out(19 downto 16);
  digit_bin(6) <= gpio_out(23 downto 20);

  hex0 <= hex(1);
  hex1 <= hex(2) when (digit_bin(2) /= X"0") and (digit_bin(3) /= X"0") else C_x;
  hex2 <= hex(3) when (digit_bin(3) /= X"0") and (digit_bin(4) /= X"0") else C_x;
  hex3 <= hex(4) when (digit_bin(4) /= X"0") and (digit_bin(5) /= X"0") else C_x;
  hex4 <= hex(5) when (digit_bin(5) /= X"0") and (digit_bin(6) /= X"0") else C_x;
  hex5 <= hex(6) when  digit_bin(6) /= X"0" else C_x;

  -- convert 4bit value into 7-segment format for digits 1-6
  gen_conv: for i in 1 to 6 generate
   Conversion_i: process (digit_bin(i))
   begin
     case digit_bin(i) is
      when X"0" => hex(i) <= C_0;
      when X"1" => hex(i) <= C_1;
      when X"2" => hex(i) <= C_2;
      when X"3" => hex(i) <= C_3;
      when X"4" => hex(i) <= C_4;
      when X"5" => hex(i) <= C_5;
      when X"6" => hex(i) <= C_6;
      when X"7" => hex(i) <= C_7;
      when X"8" => hex(i) <= C_8;
      when X"9" => hex(i) <= C_9;
      when others => hex(i) <= C_x;
    end case;
   end process;
  end generate;

end rtl;
-- --------------------------------------------------------
-- END
-- --------------------------------------------------------



