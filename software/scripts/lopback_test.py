import socket
import binascii
import os
import time

UDP_IP = "192.168.1.70"
UDP_PORT = 32
NUM_OF_FRAMES = 10000
STATISTIC_NUM = 100 # every x-th frame
DELAY = 0 # ms

def printStatus(frame_cnt, timeout_cnt, mismatch_cnt, response_error):
    print ("Frames: %i Timeout: %i Data error (local): %i Data error (remote): %i" % (frame_cnt, timeout_cnt, mismatch_cnt, response_error))
    
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setblocking(1)
sock.settimeout(2)

ramp_cnt = 1
frame_cnt = 0
timeout_cnt = 0
mismatch_cnt = 0
response_ramp = 0
response_error = 0

while(frame_cnt < NUM_OF_FRAMES):
    frame_cnt = frame_cnt + 1
    
    header    = "%04X%04X" % (0x0008, 0x0008)
    data      = "%08X%08X" % (ramp_cnt, 0x00)
    payload_raw = binascii.unhexlify(header + data)

    sock.sendto(payload_raw, (UDP_IP, UDP_PORT))
    try:
        recv_data, addr = sock.recvfrom(100)
        data_hex = binascii.hexlify(recv_data)
        response_error = int(data_hex[-4:], 16)
        response_ramp = int(data_hex[-10:-8], 16)
        if response_ramp != ramp_cnt:
            mismatch_cnt = mismatch_cnt + 1
    except socket.timeout:
        timeout_cnt = timeout_cnt + 1
        #if (ramp_cnt == 1):
        #    ramp_cnt = 255
        #else:
        #    ramp_cnt = ramp_cnt - 1
        printStatus(frame_cnt, timeout_cnt, mismatch_cnt, response_error)
        

    if (ramp_cnt < 255):
        ramp_cnt = ramp_cnt + 1
    else:
        ramp_cnt = 1
    #if (ramp_cnt == 123): ramp_cnt = ramp_cnt - 1

    if (frame_cnt % STATISTIC_NUM == 0):
        printStatus(frame_cnt, timeout_cnt, mismatch_cnt, response_error)
    if (DELAY > 0):
        time.sleep(DELAY/1000)

print ("**** FINISHED ****")
printStatus(frame_cnt, timeout_cnt, mismatch_cnt, response_error)

