import socket
import binascii
import os
import time

UDP_IP = "192.168.1.17"
UDP_PORT = 32

def printStatus(frame_cnt, timeout_cnt, mismatch_cnt, response_error):
    print ("Frames: %i Timeout: %i Data error (local): %i Data error (remote): %i" % (frame_cnt, timeout_cnt, mismatch_cnt, response_error))
    
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))

framecnt = 0

while True:
    data, addr = sock.recvfrom(100)
    data_hex = binascii.hexlify(data)
    counter = int(data_hex[-8:], 16)
    framecnt = framecnt + 1
    print "Frame: %i, Data: %i" % (framecnt, counter)
