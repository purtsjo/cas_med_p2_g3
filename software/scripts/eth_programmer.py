import socket
import time
import binascii
from thread import start_new_thread
from argparse import ArgumentParser
import re
import sys

parser = ArgumentParser(description='Ethernet Programmer')
parser.add_argument('miffile', help='Mif file name', metavar='file.mif')
args = parser.parse_args()

MCAST_GRP = '192.168.1.70'
MCAST_PORT = 32

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setblocking(1)
sock.settimeout(1)
timeout = 0
mismatch = 0

# Enable RAM access:
# ---------------------------------------
print ("Enable RAM access")
header    = "%04X%04X" % (0x0004, 0x0008)
data      = "%08X%08X" % (0x0, 0x0)
data      = header + data;
data      = binascii.unhexlify(data)
sock.sendto(data, (MCAST_GRP, MCAST_PORT))
try:
    data, addr = sock.recvfrom(100)
except socket.timeout:
    print ("ERROR: Socket timeout!")
    sys.exit(1)
time.sleep(0.1)

if 0:
    # disable RAM access:
    # ---------------------------------------
    print "Disable RAM access"
    header    = "%04X%04X" % (0x0000, 0x0008)
    data      = "%08X%08X" % (0x0, 0x0)
    data      = header + data;
    data      = binascii.unhexlify(data)
    time.sleep(0.1)
    sock.sendto(data, (MCAST_GRP, MCAST_PORT))
    try:
        data, addr = sock.recvfrom(100)
    except socket.timeout:
        print "ERROR: Socket timeout!"
        sys.exit(1)
    sys.exit(1)

# send data:
# ---------------------------------------
error = 0
retry = 0
asdf = 0
with open(args.miffile, 'r') as mif_text:
    for line in mif_text:
        pattern = '\W*(\w+)\W*:\W*(\w+).+'
        #m = re.search(pattern.decode('utf-8'), line.decode('utf-8'), re.I | re.U)
        res = re.match(pattern, line)
        if res:
            if asdf == 10000:
                continue
                #break
            asdf = asdf + 1
            address = res.group(1)
            data = res.group(2)
            header    = "%04X%04X" % (0x0002, 0x0008)
            address_c = "%08X" % (4*int(address, 16))
            data_c    = "%08X" % int(data, 16)
            payload = header + address_c + data_c
            payload_raw = binascii.unhexlify(payload)
            timeout= 0
            mismatch = 0
            while (timeout <= 3 and mismatch <= 3):
                time.sleep(0.001)
                sock.sendto(payload_raw, (MCAST_GRP, MCAST_PORT))
                try:
                    data_recv, addr = sock.recvfrom(100)
                    print address + " : " + binascii.hexlify(data_recv),
                    if (int(data, 16) == int(binascii.hexlify(data_recv)[-8:], 16) ):
                        print " > OK"
                        timeout = 33
                    else:
                        print  " > FAIL"
                        mismatch = mismatch + 1
                        error = error + 1
                except socket.timeout:
                    timeout = timeout + 1
                    print "Timeout. Retry ...";
                    retry = retry + 1
                    if (timeout > 3):
                        print "ERROR: Timeout! Too many retries!"
                        sys.exit(1)
                    else:
                        timeout = 99 #exit loop
                        continue
                        

# disable RAM access:
# ---------------------------------------
print "Disable RAM access"
header    = "%04X%04X" % (0x0000, 0x0008)
data      = "%08X%08X" % (0x0, 0x0)
data      = header + data;
data      = binascii.unhexlify(data)
time.sleep(0.1)
sock.sendto(data, (MCAST_GRP, MCAST_PORT))
try:
    data, addr = sock.recvfrom(100)
except socket.timeout:
    print "ERROR: Socket timeout!"
    sys.exit(1)
                
print "Errors: %i" % error
print "Retries: %i" % retry
