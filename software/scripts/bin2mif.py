#!/usr/bin/env python
import sys
from argparse import ArgumentParser
import binascii

parser = ArgumentParser(description='Convert Bin to Mif and Endian conversion')

parser.add_argument('infile', help='Input File', metavar='FILE')
parser.add_argument('outfile', help='Output Assembly File', metavar='FILE')

parser.add_argument('-d', '--depth', type=int, default=1000)
parser.add_argument('-w', '--width', type=int, default=32)
parser.add_argument('-r', '--radix', type=str, default="HEX")
parser.add_argument('-dr', '--data-radix', metavar='RADIX', type=str, default='HEX')
parser.add_argument('-m', '--max-instructions', type=int, default='63')

args = parser.parse_args()

with open(args.infile, 'rb') as binary_text:
  with open(args.outfile, 'w') as mif_file:
    mif_file.write('DEPTH={};\n'.format(args.depth))
    mif_file.write('WIDTH = {};\n'.format(args.width))
    mif_file.write('ADDRESS_RADIX = {};\n'.format(args.radix))
    mif_file.write('DATA_RADIX = {};\n\n'.format(args.data_radix))
    mif_file.write('CONTENT\n')
    mif_file.write('BEGIN\n')

    inst_num = 0
    #for instruction in binary_text:
    word = binary_text.read(4)
    while word != "":
        # Do stuff with byte.
        hexstr = binascii.b2a_hex(word)
        # endianess conversion:
        instr = hexstr[6:8] + hexstr[4:6] + hexstr[2:4] + hexstr[0:2]
        #instr = hexstr
        word = binary_text.read(4)
        mif_file.write('{0:x} : {1};\n'.format(inst_num, instr))
        inst_num += 1

    while inst_num < args.depth:
        mif_file.write('{0:x} : 00000000;\n'.format(inst_num))
        inst_num += 1

    mif_file.write("\nEND;\n")
