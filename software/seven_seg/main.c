#include <stdint.h>
#include <uart.h>

volatile uint32_t *SPI_RXTX         = (uint32_t*)0x80002000;
volatile uint32_t *SPI_CTRL         = (uint32_t*)0x80002010;
volatile uint32_t *SPI_DIV          = (uint32_t*)0x80002014;
volatile uint32_t *SPI_SS           = (uint32_t*)0x80002018;

volatile uint32_t *GPIO_CLEAR       = (uint32_t*)0x80001000;
volatile uint32_t *GPIO_SET         = (uint32_t*)0x80001004;


/*#define ADC_CH0_SETUP 0x00008310*/
#define ADC_CH0_SETUP 0x00008300
#define SPI_CTRL_SETUP 0x00002010
#define SPI_DIV_SETUP 0x00000004
#define SPI_SS_SETUP 0x00000001
#define SPI_GO_BSY 0x00002110
#define SPI_BSY_MASK 0x00000100


/*void segment_set_int(unsigned int value2)
{
  volatile unsigned int digits = 0;
  volatile unsigned int value = value2;
  volatile unsigned int shift = 0;
  volatile unsigned int i = 0;
    
  *(volatile unsigned int *) ( GPIO_CLEAR ) = 0x00ffffff;
  if (value < 1000000){
		for(i=0;i<6;i++){
		  if (shift == 0){
			  digits = (value % 10);
		  }
		  else{
			  digits += ((value % 10)*shift*16);
		  }
		  value /= 10;
		  shift++;
		}
	}
	else{
		digits = 0x00ffffff;
	}
  *(volatile unsigned int *) ( GPIO_SET ) = digits;  
}*/

/*void sleep_us(unsigned int delay)
{ 
    volatile unsigned int current_time = rdtime();
    if ((0xffffffff - current_time) < delay){
      while(((delay - (0xffffffff - current_time)) > rdtime()) || (current_time < rdtime()));
    }
    else{
		  while((current_time + delay) <= rdtime());
		}      
}*/

void delay(uint32_t v)
{
  volatile uint32_t i;
  for(i=0;i<v;i++);
}

void start_spi_transfer(void)
{
  *SPI_CTRL = SPI_GO_BSY;
  while((*SPI_CTRL & SPI_BSY_MASK) == SPI_BSY_MASK);
}

void init_spi(void)
{
  *SPI_DIV  = SPI_DIV_SETUP;
  *SPI_CTRL = SPI_CTRL_SETUP;
  *SPI_SS   = SPI_SS_SETUP;
  
  *SPI_RXTX = 0x0000ffff;
  start_spi_transfer();
  *SPI_RXTX = 0x0000ffff;
  start_spi_transfer();
  
  *SPI_RXTX = ADC_CH0_SETUP;
  start_spi_transfer();
}

uint32_t receive_spi(void)
{
  *SPI_RXTX = 0;
  start_spi_transfer();
  return *SPI_RXTX;
}


main()
{
  volatile uint32_t digits = 0;
  volatile uint32_t value = 0;
  volatile uint32_t value_r = 0;
  volatile uint32_t shift = 0;
  volatile uint32_t rest = 0;
  volatile uint32_t i = 0;
  
  uint32_t adc_value = 0;
  
  uint32_t counter = 0;
  uint32_t current_time = 0;
  volatile uint32_t c;
  
  uart_init_hw();
  init_spi();
  uart_write_byte((uint8_t)receive_spi());
  /**BAUD_DIVIDER_REG = CALC_BAUD(BAUDRATE);*/
  
  *GPIO_SET = 4369;
	*GPIO_SET = (1<<25);
	for(c=0;c<2000000;c++);
	*GPIO_CLEAR = 0x00ffffff;
	*GPIO_SET = 9;
	*GPIO_SET = (1<<26);
	for(c=0;c<2000000;c++);
	*GPIO_CLEAR = 0x00ffffff;
	*GPIO_SET = 10;
	*GPIO_SET = (1<<27);
	for(c=0;c<2000000;c++);
	*GPIO_CLEAR = 0x00ffffff;
	*GPIO_SET = 17;
	*GPIO_SET = (1<<28);
	for(c=0;c<2000000;c++);
	*GPIO_CLEAR = 0x00ffffff;
	*GPIO_SET = 16;
	*GPIO_SET = (1<<29);
	
	for(c=0;c<2000000;c++);
	/*segment_set_int(1234);*/
	/*puts("Starting to work\n");*/
	
	for(;;){
	  delay(400000);
		for(c=0;c<2000000;c++);
		
		if (counter > 999999){
			counter = 0;
		}
		else{
			counter++;
		}
    value = counter;
    /*TX_REG = (uint8_t)counter;*/
    
    adc_value = (receive_spi() & 0x0000fff0)>>4;
    uart_write_byte((uint8_t)adc_value);
    
    *GPIO_CLEAR = 0x00ffffff;
    shift = 0;
    digits = 0;
   
		
		/*for(i=0;i<6;i++)
		{
		  value_r = (value / 10);
		  uart_write_byte((uint8_t)value_r);
		  for(c=0;c<2000000;c++);
	    rest = (value - (value_r*10));
	    uart_write_byte((uint8_t)rest);
	    for(c=0;c<2000000;c++);
		  if (shift == 0){
			  digits = digits | rest;
		  }
		  else{
			  digits = digits | ((rest*shift*16)&(15*shift*16));
		  }
		  uart_write_byte((uint8_t)digits);
	    for(c=0;c<2000000;c++);
		  shift++;
		  uart_write_byte((uint8_t)shift);
	    for(c=0;c<2000000;c++);
		  value = value_r;
	  }
	  if (shift == 6){
			  *GPIO_SET = digits;
		  }
		else{
			  *GPIO_SET = 4369;
		  }*/
		  
		/*uart_write_byte(uart_read_byte());*/
    
	}
}
