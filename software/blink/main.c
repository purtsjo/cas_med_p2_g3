#include <stdint.h>

volatile uint32_t *GPIO_CLEAR   = (uint32_t*)0x80001000;
volatile uint32_t *GPIO_SET     = (uint32_t*)0x80001004;

void delay(uint32_t v)
{
  volatile uint32_t i;
  for(i=0;i<v;i++);
}

main()
{
	for(;;){
		*GPIO_SET = 0x01000000;
		delay(2000000);
		*GPIO_CLEAR = 0x01000000;
		delay(2000000);
	}
}
