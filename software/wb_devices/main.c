#include <stdint.h>
#include <uart.h>

volatile uint32_t *ETH_CTL          = (uint32_t*)0x80003000;
volatile uint32_t *ETH_CNT          = (uint32_t*)0x80003004;
volatile uint32_t *ETH_ERR          = (uint32_t*)0x80003008;
volatile uint32_t *ETH_OUT          = (uint32_t*)0x8000300C;

volatile uint32_t *SPI_RXTX         = (uint32_t*)0x80002000;
volatile uint32_t *SPI_CTRL         = (uint32_t*)0x80002010;
volatile uint32_t *SPI_DIV          = (uint32_t*)0x80002014;
volatile uint32_t *SPI_SS           = (uint32_t*)0x80002018;

volatile uint32_t *GPIO_CLEAR       = (uint32_t*)0x80001000;
volatile uint32_t *GPIO_SET         = (uint32_t*)0x80001004;


#define ADC_CH0_SETUP  0x00008310
#define SPI_CTRL_SETUP 0x00002010
#define SPI_DIV_SETUP  0x00000004
#define SPI_SS_SETUP   0x00000001
#define SPI_GO_BSY     0x00002110
#define SPI_BSY_MASK   0x00000100


void delay(uint32_t v)
{
  volatile uint32_t i;
  for(i=0;i<v;i++);
}

void start_spi_transfer(void)
{
  *SPI_CTRL = SPI_GO_BSY;
  while((*SPI_CTRL & SPI_BSY_MASK) == SPI_BSY_MASK);
}

void init_spi(void)
{
  *SPI_DIV  = SPI_DIV_SETUP;
  *SPI_CTRL = SPI_CTRL_SETUP;
  *SPI_SS   = SPI_SS_SETUP;
  
  *SPI_RXTX = 0x0000ffff;
  start_spi_transfer();
  *SPI_RXTX = 0x0000ffff;
  start_spi_transfer();
  
  *SPI_RXTX = ADC_CH0_SETUP;
  start_spi_transfer();
}

uint32_t receive_spi(void)
{
  *SPI_RXTX = 0;
  start_spi_transfer();
  return *SPI_RXTX;
}

uint8_t echo(void)
{
  uint8_t key = 0;
  key = uart_read_byte();
  if (key > 0x50 && key < 0x7b){
    uart_write_byte(key);
  }
  return key;
}

uint32_t divide(uint32_t nu, uint32_t de) {

    uint32_t temp = 1;
    uint32_t quotient = 0;

    while (de <= nu) {
        de <<= 1;
        temp <<= 1;
    }
    /*printf("%d %d\n",de,temp,nu);*/
    while (temp > 1) {
        de >>= 1;
        temp >>= 1;

        if (nu >= de) {
            nu -= de;
            /*printf("%d %d\n",quotient,temp);*/
            quotient += temp;
        }
    }
    return quotient;
}

uint32_t remainder(uint32_t base, uint32_t mod) {

    uint32_t temp = 0;
    uint32_t rem = 0;

    temp = divide(base,mod);
    rem = base -(temp*mod);

    return rem;
}

void segment_set_int(uint32_t value)
{
  uint32_t digits = 0;
  uint32_t shift = 0;
  uint32_t i = 0;
    
  *GPIO_CLEAR = 0x00ffffff;
  if (value < 1000000){
		for(i=0;i<6;i++){
		  digits |= (remainder(value,10)<<shift);
		  value = divide(value,10);
		  shift+=4;
		}
	}
	else{
		digits = 0x00ffffff;
	}
  *GPIO_SET = digits & 0x00ffffff;  
}


main()
{  
  uint32_t adc_value = 0;
  uint8_t valid_cmd = 0;
  uint32_t counter = 0;
  uint32_t count_t = 0;
  uint32_t e_counter = 0;

  uart_init_hw();
  init_spi();
  
  *GPIO_SET = 0x00ffffff;
	*GPIO_SET = (1<<25);
	delay(2000000);
	*GPIO_SET = (1<<26);
	delay(2000000);
	*GPIO_SET = (1<<27);
	delay(2000000);
	*GPIO_SET = (1<<28);
	delay(2000000);
	*GPIO_SET = (1<<29);
	
	for(;;){
	  
		*GPIO_CLEAR = 0x00ffffff;
		if (counter == 999999){
			counter = 0;
		}
		else{
			counter++;
		}
		segment_set_int(counter);
    valid_cmd = echo();
    
    if (valid_cmd == 'g'){
      while(valid_cmd != 'x'){
        valid_cmd = echo();
        *GPIO_SET = 0x01000000;
		    delay(1000000);
		    *GPIO_CLEAR = 0x01000000;
		    delay(1000000);
		  }
    }
    if (valid_cmd == 'a'){
      while(valid_cmd != 'x'){
        valid_cmd = echo();
        adc_value = (receive_spi() & 0x00000fff);
        uart_write_byte((uint8_t)adc_value);
        segment_set_int(adc_value);
		    delay(2000000);
		  }
    }
    if (valid_cmd == 'e'){
      while(valid_cmd != 'x'){
        valid_cmd = echo();
        if (valid_cmd == 'l'){
          uart_write_byte((uint8_t)*ETH_CNT);
          segment_set_int(*ETH_CNT);
		    }
		    if (valid_cmd == 'e'){
          uart_write_byte((uint8_t)*ETH_ERR);
          segment_set_int(*ETH_ERR);
		    }
		    if (valid_cmd == 's'){
		      e_counter = 0;
		      while(valid_cmd != 'x'){
            valid_cmd = echo();
		        if (e_counter == 0xffffffff){
			        e_counter = 0;
		        }
		        else{
			        e_counter++;
		        }
            *ETH_OUT = e_counter;
            segment_set_int(e_counter);
            delay(500000);
		      }
		    }
		    if (valid_cmd == 'a'){
		      while(valid_cmd != 'x'){
            valid_cmd = echo();
		        adc_value = (receive_spi() & 0x00000fff);
            *ETH_OUT = adc_value;
            segment_set_int(adc_value);
            delay(500000);
		      }
		    }
		    delay(2000000);
		  }
    }
    delay(3000000);
	}
}
