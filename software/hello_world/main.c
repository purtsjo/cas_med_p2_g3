#include <stdlib.h>

volatile unsigned int *TX_REG = (unsigned int*)0x80000008;
volatile unsigned int *BAUD_DIVIDER_REG = (unsigned int*)0x80000004;
volatile unsigned int *STATUS_REG = (unsigned int*)0x80000000;

#define BASE_CLOCK 100000000
#define BAUDRATE 115200

#define CALC_BAUD(baudrate) ( ((( (unsigned int)baudrate << 12)) + (BASE_CLOCK >> 8)) / (BASE_CLOCK >> 7) )

const unsigned char *txt = "Hello World\n";
unsigned int txtlen = 12;
unsigned char *ptr;
unsigned char *p;

main()
{
	*BAUD_DIVIDER_REG = CALC_BAUD(BAUDRATE);
    //*TX_REG = 0xB7;
    
    p = malloc(16);
    printf("a");
    
	ptr = txt;
    int i=0;
    for(i=0;i<txtlen;i++) {
		*(TX_REG) = *(ptr++);
		while (*STATUS_REG & 0x01);
	}
	
	for(;;);
}
