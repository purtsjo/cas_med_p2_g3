#include <stdlib.h>
#include <stdint.h>
#include "pico_defines.h"
#include "pico_stack.h"
#include "pico_ipv4.h"
#include "pico_zmq.h"
#include "board.h"

void zmq_cb(ZMQ z);
int pico_eth_send(struct pico_device *dev, void *buf, int len);
char buf[255]= "";
int zmq_connected = 0;

void zmq_cb(ZMQ z)
{
	dbg("ZMQ connected!\r\n");
    zmq_connected = 1;
}

int main(void)
{
    //BSP_Init();

    char ipaddr[]="192.168.2.150";
    struct pico_ip4 my_eth_addr, netmask;
    struct pico_device *pico_dev_eth;

    uint8_t mac[6] = {0x00,0x00,0x00,0x12,0x34,0x56};

    pico_stack_init();

    pico_dev_eth = (struct pico_device *) pico_eth_create("eth", mac);
    if (!pico_dev_eth)
	return 255;       
	/*while (1); */

    pico_string_to_ipv4(ipaddr, &my_eth_addr.addr);
    pico_string_to_ipv4("255.255.255.0", &netmask.addr);
    pico_ipv4_link_add(pico_dev_eth, my_eth_addr, netmask);

    ZMQ zmq_sock;
    zmq_sock = zmq_publisher(1234, zmq_cb);

    for (;;) {
        pico_stack_tick();
        if ((PICO_TIME_MS()%1000)==0) /* & 1023 cpu leistung sparen */
        {
            int temp = 22;
            int hum = 66;
            int len;
            /* Prepare string for ZMQ */
            sprintf(buf, "{\"msg\":\"data\",\"temp\":%i,\"hum\":%i}\n", temp, hum);
            len = strlen(buf);
    		/* Publish stuff via ZMQ */
            zmq_send(zmq_sock, buf, len);
        }
        PICO_IDLE();
    }
}

