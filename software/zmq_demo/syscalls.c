#include <sys/types.h>
#include <sys/stat.h>

caddr_t sbrk(int incr) {
  extern char _end;		/* Defined by the linker */
  static char *heap_end;
  char *prev_heap_end;
 
  if (heap_end == 0) {
    heap_end = &_end;
  }
  prev_heap_end = heap_end;
  //if (heap_end + incr > stack_ptr) {
  //  write (1, "Heap and stack collision\n", 25);
  //  abort ();
  //}

  heap_end += incr;
  return (caddr_t) prev_heap_end;
}

int close(int file) {
  return -1;
}

int open(const char *name, int flags, int mode) {
  return -1;
}

int isatty(int file) {
  return 1;
}

int fstat(int file, struct stat *st) {
  st->st_mode = S_IFCHR;
  return 0;
}

int lseek(int file, int ptr, int dir) {
  return 0;
}

int read(int file, char *ptr, int len) {
  return 0;
}

int write(int file, char *ptr, int len) {
  int todo;

  for (todo = 0; todo < len; todo++) {
    //outbyte (*ptr++);
    *ptr++;
  }
  return len;
}
