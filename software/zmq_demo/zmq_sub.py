import time
import zmq
import struct
import sys

def main():
    connect_to = "tcp://192.168.2.150:1234"

    print "Connecting to SN82000..."

    ctx = zmq.Context()
    s = ctx.socket(zmq.SUB)
    s.connect(connect_to)
    print "   Done."
    s.setsockopt(zmq.SUBSCRIBE,'')

    print "Getting data pushed from SN8200..."
    while True:
        a = s.recv()
        print "rcv> ",
        print a,
    print "   Done."

    time.sleep(1.0)

if __name__ == "__main__":
    main()

